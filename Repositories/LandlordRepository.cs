using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data.Entity;
using DTO;
using AutoMapper;

namespace Repositories
{
    public class LandlordRepository : ILandlordRepository
    {
        private localdb db = new localdb();

        public LandlordRepository()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<OccupancyProfile>();
            });
        }

        public List<DTO.Landlord> GetAll()
        {
            var list = new List<DTO.Landlord>();

            var landlords = from u in db.Landlords
                            where u.Deleted == false
                            select u;

            foreach (var landlord in landlords)
            {
                DTO.Landlord model = Mapper.Map<Models.Landlord, DTO.Landlord>(landlord);
                list.Add(model);
            }

            return list;
        }

        public List<DTO.Landlord> GetAll(int currentPage, int pageSize)
        {
            var list = new List<DTO.Landlord>();

            var landlords = db.Landlords.Include("XXX").Include("XXX").Include("ApartmentType").Include("Occupiers").Include("Apartment").Include("OccupierType")
                .OrderByDescending(u => u.ID)
                .Skip(currentPage)
                .Take(pageSize);

            foreach (var landlord in landlords)
            {
                DTO.Landlord model = Mapper.Map<Models.Landlord, DTO.Landlord>(landlord);
                list.Add(model);
            }

            return list;
        }

        public int LandlordCount()
        {
            var count = db.Landlords.Count(x => x.Deleted == false);
            return count;
        }

        public DTO.Landlord Get(int ID)
        {
            var landlord = db.Landlords
               .FirstOrDefault(u => u.ID == ID && u.Deleted == false);

            DTO.Landlord landlordDTO = Mapper.Map<Models.Landlord, DTO.Landlord>(landlord);

            return landlordDTO;
        }

        public int Add(DTO.Landlord landlord)
        {
            // dto to poco
            Models.Landlord landlordModel = Mapper.Map<DTO.Landlord, Models.Landlord>(landlord);
            landlordModel.Deleted = false;
            landlordModel.DateCreated = DateTime.Now;
            landlordModel.DateUpdated = DateTime.Now;
            db.Landlords.Add(landlordModel);
            Save();
            return landlordModel.ID;
        }

        public bool Update(DTO.Landlord landlord)
        {

            try
            {
                // map dto to empty poco
                Models.Landlord ExistingLandlord = new Models.Landlord();
                Mapper.Map<DTO.Landlord, Models.Landlord>(landlord, ExistingLandlord);

                // get the existing record
                Models.Landlord ExistingLandlord2 = db.Landlords.FirstOrDefault(u => u.ID == landlord.ID);

                // copy the new data poco to the existing record and save to db
                db.Entry(ExistingLandlord2).CurrentValues.SetValues(ExistingLandlord);
                ExistingLandlord2.DateUpdated = DateTime.Now;
                db.Entry(ExistingLandlord2).Property(x => x.DateCreated).IsModified = false;
                db.Entry(ExistingLandlord2).Property(x => x.Deleted).IsModified = false;
                Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int ID)
        {
            bool result = true;
            try
            {
                Models.Landlord landlord = db.Landlords.FirstOrDefault(u => u.ID == ID);
                landlord.Deleted = true;
                Save();
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public void Save()
        {
            db.SaveChanges();
        }

    }
}