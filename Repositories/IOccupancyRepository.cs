﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace Repositories
{
    public interface IOccupancyRepository
    {

        int Add(Occupancy opccupancy);
        bool Delete(int ID);
        List<Occupancy> GetAll();
        List<Occupancy> GetAll(int currentPage, int pageSize);
        Occupancy Get(int ID);
        void Save();
        bool Update(Occupancy occupancy);
        int OccupancyCount();

    }
}
