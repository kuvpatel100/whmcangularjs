using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace Repositories
{
    public interface IOccupierRepository
    {
		int Add(Occupier occupier);
        bool Delete(int ID);
        List<Occupier > GetAll();
        List<Occupier > GetAll(int currentPage, int pageSize);
        Occupier  Get(int ID);
        void Save();
        bool Update(Occupier occupier);
        int OccupierCount();
    }
}