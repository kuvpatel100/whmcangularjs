
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data.Entity;
using DTO;
using AutoMapper;

namespace Repositories
{
	public class OccupancyNoteRepository : IOccupancyNoteRepository
    {
        private localdb db = new localdb();

        public OccupancyNoteRepository()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<OccupancyProfile>();
            });
        }

        public List<DTO.OccupancyNote> GetAll()
        {
            var list = new List<DTO.OccupancyNote>();

            var occupancynotes = from u in db.OccupancyNotes
                        where u.Deleted == false
                        select u;

            foreach (var occupancynote in occupancynotes)
            {
                DTO.OccupancyNote model = Mapper.Map<Models.OccupancyNote, DTO.OccupancyNote>(occupancynote);
            }

            return list;
        }

        public List<DTO.OccupancyNote> GetAll(int currentPage, int pageSize)
        {
            var list = new List<DTO.OccupancyNote>();

            var occupancynotes = db.OccupancyNotes.Include("XXX").Include("XXX").Include("ApartmentType").Include("Occupiers").Include("Apartment").Include("OccupierType")
                .OrderByDescending(u => u.ID)
                .Skip(currentPage)
                .Take(pageSize);

            foreach (var occupancynote in occupancynotes)
            {
                DTO.OccupancyNote model = Mapper.Map<Models.OccupancyNote, DTO.OccupancyNote>(occupancynote);
                list.Add(model);
            }

            return list;
        }

        public int OccupancyNoteCount()
        {
            var count = db.OccupancyNotes.Count(x => x.Deleted == false);
            return count;
        }

        public DTO.OccupancyNote Get(int ID)
        {
            var occupancynote = db.OccupancyNotes.Include("XXX").Include("XXX").Include("XXX").Include("Occupiers").Include("Apartment").Include("OccupierType")
               .FirstOrDefault(u => u.ID == ID);

            DTO.OccupancyNote occupancynoteDTO = Mapper.Map<Models.OccupancyNote, DTO.OccupancyNote>(occupancynote);

            return occupancynoteDTO;
        }

        public int Add(DTO.OccupancyNote occupancynote)
        {
            // dto to poco
            Models.OccupancyNote occupancynoteModel = Mapper.Map<DTO.OccupancyNote, Models.OccupancyNote>(occupancynote);
            occupancynoteModel.Deleted = false;
            db.OccupancyNotes.Add(occupancynoteModel);
            Save();
            return occupancynoteModel.ID;
        }

        public bool Update(DTO.OccupancyNote occupancynote)
        {
            // map dto to empty poco
            Models.OccupancyNote ExistingOccupancyNote = new Models.OccupancyNote();
            Mapper.Map<DTO.OccupancyNote, Models.OccupancyNote>(occupancynote, ExistingOccupancyNote);

            // get the existing record
            Models.OccupancyNote ExistingOccupancyNote2 = db.OccupancyNotes.FirstOrDefault(u => u.ID == occupancynote.ID);

            // copy the new data poco to the existing record and save to db
            db.Entry(ExistingOccupancyNote2).CurrentValues.SetValues(ExistingOccupancyNote);

            Save();
            return true;
        }

        public bool Delete(int ID)
        {
            bool result = true;
            try
            {
                db.OccupancyNotes.Remove(db.OccupancyNotes.Single(f => f.ID == ID));
                Save();
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public void Save()
        {
            db.SaveChanges();
        }

    }
}