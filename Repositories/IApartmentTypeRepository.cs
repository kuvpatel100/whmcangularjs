using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace Repositories
{
    public interface IApartmentTypeRepository
    {
		int Add(ApartmentType apartmentType);
        bool Delete(int ID);
        List<ApartmentType > GetAll();
        List<ApartmentType > GetAll(int currentPage, int pageSize);
        ApartmentType  Get(int ID);
        void Save();
        bool Update(ApartmentType apartmentType);
        int ApartmentTypeCount();
    }
}