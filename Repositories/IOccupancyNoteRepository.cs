using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace Repositories
{
    public interface IOccupancyNoteRepository
    {
		int Add(OccupancyNote occupancyNote);
        bool Delete(int ID);
        List<OccupancyNote > GetAll();
        List<OccupancyNote > GetAll(int currentPage, int pageSize);
        OccupancyNote  Get(int ID);
        void Save();
        bool Update(OccupancyNote occupancyNote);
        int OccupancyNoteCount();
    }
}