using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace Repositories
{
    public interface ILandlordRepository
    {
		int Add(Landlord landlord);
        bool Delete(int ID);
        List<Landlord > GetAll();
        List<Landlord > GetAll(int currentPage, int pageSize);
        Landlord  Get(int ID);
        void Save();
        bool Update(Landlord landlord);
        int LandlordCount();
    }
}