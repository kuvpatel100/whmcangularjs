using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data.Entity;
using DTO;
using AutoMapper;

namespace Repositories
{
	public class OccupierRepository : IOccupierRepository
    {
        private localdb db = new localdb();

        public OccupierRepository()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<OccupancyProfile>();
            });
        }

        public List<DTO.Occupier> GetAll()
        {
            var list = new List<DTO.Occupier>();

            var occupiers = from u in db.Occupiers
                        where u.Deleted == false
                        select u;

            foreach (var occupier in occupiers)
            {
                DTO.Occupier model = Mapper.Map<Models.Occupier, DTO.Occupier>(occupier);
            }

            return list;
        }

        public List<DTO.Occupier> GetAll(int currentPage, int pageSize)
        {
            var list = new List<DTO.Occupier>();

            var occupiers = db.Occupiers.Include("XXX").Include("XXX").Include("ApartmentType").Include("Occupiers").Include("Apartment").Include("OccupierType")
                .OrderByDescending(u => u.ID)
                .Skip(currentPage)
                .Take(pageSize);

            foreach (var occupier in occupiers)
            {
                DTO.Occupier model = Mapper.Map<Models.Occupier, DTO.Occupier>(occupier);
                list.Add(model);
            }

            return list;
        }

        public int OccupierCount()
        {
            var count = db.Occupiers.Count(x => x.Deleted == false);
            return count;
        }

        public DTO.Occupier Get(int ID)
        {
            var occupier = db.Occupiers.Include("XXX").Include("XXX").Include("XXX").Include("Occupiers").Include("Apartment").Include("OccupierType")
               .FirstOrDefault(u => u.ID == ID);

            DTO.Occupier occupierDTO = Mapper.Map<Models.Occupier, DTO.Occupier>(occupier);

            return occupierDTO;
        }

        public int Add(DTO.Occupier occupier)
        {
            // dto to poco
            Models.Occupier occupierModel = Mapper.Map<DTO.Occupier, Models.Occupier>(occupier);
            occupierModel.Deleted = false;
            db.Occupiers.Add(occupierModel);
            Save();
            return occupierModel.ID;
        }

        public bool Update(DTO.Occupier occupier)
        {
            // map dto to empty poco
            Models.Occupier ExistingOccupier = new Models.Occupier();
            Mapper.Map<DTO.Occupier, Models.Occupier>(occupier, ExistingOccupier);

            // get the existing record
            Models.Occupier ExistingOccupier2 = db.Occupiers.FirstOrDefault(u => u.ID == occupier.ID);

            // copy the new data poco to the existing record and save to db
            db.Entry(ExistingOccupier2).CurrentValues.SetValues(ExistingOccupier);

            Save();
            return true;
        }

        public bool Delete(int ID)
        {
            bool result = true;
            try
            {
                db.Occupiers.Remove(db.Occupiers.Single(f => f.ID == ID));
                Save();
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public void Save()
        {
            db.SaveChanges();
        }

    }
}