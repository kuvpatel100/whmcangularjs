using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data.Entity;
using DTO;
using AutoMapper;

namespace Repositories
{
	public class EstateAgentRepository : IEstateAgentRepository
    {
        private localdb db = new localdb();

        public EstateAgentRepository()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<OccupancyProfile>();
            });
        }

        public List<DTO.EstateAgent> GetAll()
        {
            var list = new List<DTO.EstateAgent>();

            var estateagents = from u in db.EstateAgents
                        where u.Deleted == false
                        select u;

            foreach (var estateagent in estateagents)
            {
                DTO.EstateAgent model = Mapper.Map<Models.EstateAgent, DTO.EstateAgent>(estateagent);
                list.Add(model);
            }

            return list;
        }

        public List<DTO.EstateAgent> GetAll(int currentPage, int pageSize)
        {
            var list = new List<DTO.EstateAgent>();

            var estateagents = db.EstateAgents.Include("XXX").Include("XXX").Include("ApartmentType").Include("Occupiers").Include("Apartment").Include("OccupierType")
                .OrderByDescending(u => u.ID)
                .Skip(currentPage)
                .Take(pageSize);

            foreach (var estateagent in estateagents)
            {
                DTO.EstateAgent model = Mapper.Map<Models.EstateAgent, DTO.EstateAgent>(estateagent);
                list.Add(model);
            }

            return list;
        }

        public int EstateAgentCount()
        {
            var count = db.EstateAgents.Count(x => x.Deleted == false);
            return count;
        }

        public DTO.EstateAgent Get(int ID)
        {
            var estateagent = db.EstateAgents
               .FirstOrDefault(u => u.ID == ID);

            DTO.EstateAgent estateagentDTO = Mapper.Map<Models.EstateAgent, DTO.EstateAgent>(estateagent);

            return estateagentDTO;
        }

        public int Add(DTO.EstateAgent estateagent)
        {
            // dto to poco
            Models.EstateAgent estateagentModel = Mapper.Map<DTO.EstateAgent, Models.EstateAgent>(estateagent);
            estateagentModel.Deleted = false;
            estateagentModel.DateCreated = DateTime.Now;
            estateagentModel.DateUpdated = DateTime.Now;
            db.EstateAgents.Add(estateagentModel);
            Save();
            return estateagentModel.ID;
        }


        public bool Update(DTO.EstateAgent estateagent)
        {
            try
            {
                // map dto to empty poco
                Models.EstateAgent ExistingEstateAgent = new Models.EstateAgent();
                Mapper.Map<DTO.EstateAgent, Models.EstateAgent>(estateagent, ExistingEstateAgent);

                // get the existing record
                Models.EstateAgent ExistingEstateAgent2 = db.EstateAgents.FirstOrDefault(u => u.ID == estateagent.ID);

                // copy the new data poco to the existing record and save to db
                db.Entry(ExistingEstateAgent2).CurrentValues.SetValues(ExistingEstateAgent);
                ExistingEstateAgent2.DateUpdated = DateTime.Now;
                db.Entry(ExistingEstateAgent2).Property(x => x.DateCreated).IsModified = false;
                db.Entry(ExistingEstateAgent2).Property(x => x.Deleted).IsModified = false;
                Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int ID)
        {
            bool result = true;
            try
            {
                Models.EstateAgent estateagent = db.EstateAgents.FirstOrDefault(u => u.ID == ID);
                estateagent.Deleted = true;
                Save();
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public void Save()
        {
            db.SaveChanges();
        }

    }
}