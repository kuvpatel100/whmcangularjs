
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace Repositories
{
    public interface IEstateAgentRepository
    {
		int Add(EstateAgent estateAgent);
        bool Delete(int ID);
        List<EstateAgent > GetAll();
        List<EstateAgent > GetAll(int currentPage, int pageSize);
        EstateAgent  Get(int ID);
        void Save();
        bool Update(EstateAgent estateAgent);
        int EstateAgentCount();
    }
}