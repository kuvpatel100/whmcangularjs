﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data.Entity;
using DTO;
using AutoMapper;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core;
using System.Collections.ObjectModel;

namespace Repositories
{
    public class OccupancyRepository : IOccupancyRepository
    {
        private localdb db = new localdb();

        public OccupancyRepository()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<OccupancyProfile>();
            });
        }

        public List<DTO.Occupancy> GetAll()
        {
            var list = new List<DTO.Occupancy>();

            var occupancies = from u in db.Occupancies
                        where u.Deleted == false
                        select u;

            foreach (var occupancy in occupancies)
            {
                DTO.Occupancy model = Mapper.Map<Models.Occupancy, DTO.Occupancy>(occupancy);
                list.Add(model);
            }

            return list;
        }

        public List<DTO.Occupancy> GetAll(int currentPage, int pageSize)
        {
            var list = new List<DTO.Occupancy>();

            var occupancies = db.Occupancies.Include("Landlord").Include("EstateAgent").Include("Occupiers").Include("Apartment").Include("OccupierType")
                .OrderByDescending(u => u.ID)
                .Skip(currentPage)
                .Take(pageSize);

            foreach (var occupancy in occupancies)
            {
                DTO.Occupancy model = Mapper.Map<Models.Occupancy, DTO.Occupancy>(occupancy);
                list.Add(model);
            }

            return list;
        }

        public int OccupancyCount()
        {
            var count = db.Occupancies.Count(x => x.Deleted == false);
            return count;
        }

        public DTO.Occupancy Get(int ID)
        {
            var occupancy = db.Occupancies.Include("Landlord").Include("EstateAgent").Include("Occupiers").Include("Apartment").Include("OccupierType")
               .FirstOrDefault(u => u.ID == ID);

            DTO.Occupancy occupancyDTO = Mapper.Map<Models.Occupancy, DTO.Occupancy>(occupancy);

            return occupancyDTO;
        }

        public int Add(DTO.Occupancy occupancy)
        {
            int ID = 0;
            try
            {
                // dto to poco
                Models.Occupancy occupancyModel = Mapper.Map<DTO.Occupancy, Models.Occupancy>(occupancy);
                // set default values
                occupancyModel.Deleted = false;
                occupancyModel.DateUpdated = DateTime.Now;
                occupancyModel.DateCreated = DateTime.Now;   // ef does not recognise default sql values therefore set a default
                occupancyModel.LeavingDate = null;

                if (occupancyModel.EstateAgentID <= 0) {
                    occupancyModel.EstateAgent.DateCreated = DateTime.Now;
                    occupancyModel.EstateAgent.DateUpdated = DateTime.Now;
                   // occupancyModel.EstateAgent.ID = (int)occupancyModel.EstateAgentID;
                }
                else { 
                    occupancyModel.EstateAgent = null;
                }

                if (occupancyModel.LandlordID <= 0) { 
                    occupancyModel.Landlord.DateCreated = DateTime.Now;
                    occupancyModel.Landlord.DateUpdated = DateTime.Now;
                    //occupancyModel.Landlord.ID = (int)occupancyModel.LandlordID;
                }
                else { 
                    occupancyModel.Landlord = null;
                }

                // set up the vehicle records and remove blank if only 1 car
                foreach (Models.Vehicle item in occupancyModel.Vehicles)
                {
                    if ( item.Registration.Trim().Length > 0)
                    {
                        item.DateCreated = DateTime.Now;
                        item.DateUpdated = DateTime.Now;
                        item.Deleted = false;
                    }
                    else
                    {
                        occupancyModel.Vehicles.Remove(item);   // blank so remove i.e. occupier has only one car
                    }
                }

                // occupiers
                foreach (Models.Occupier item in occupancyModel.Occupiers)
                {
                    item.DateCreated = DateTime.Now;
                    item.DateUpdated = DateTime.Now;
                    item.Deleted = false;
                }

                // prevent ef from inserting child lookup objects
                //occupancyModel.Occupiers = n;
                occupancyModel.Apartment = null;
                //occupancyModel.Vehicles = null;
                occupancyModel.OccupierType = null;

                db.Occupancies.Add(occupancyModel);
                Save();
                ID = occupancyModel.ID;
            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
            }
            return ID;
        }

        public bool Update(DTO.Occupancy occupancy)
        {
            // map dto to empty poco
            Models.Occupancy ExistingOccupancy = new Models.Occupancy();
            Mapper.Map<DTO.Occupancy, Models.Occupancy>(occupancy, ExistingOccupancy);

            // get the existing record
            Models.Occupancy ExistingOccupancy2 = db.Occupancies.FirstOrDefault(u => u.ID == occupancy.ID);

            // copy the new data poco to the existing record and save to db
            db.Entry(ExistingOccupancy2).CurrentValues.SetValues(ExistingOccupancy);

            Save();
            return true;
        }

        public bool Delete(int ID)
        {
            bool result = true;
            try
            {
                db.Occupancies.Remove(db.Occupancies.Single(f => f.ID == ID));
                Save();
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public void Save()
        {
            db.SaveChanges();
        }

    }
}
