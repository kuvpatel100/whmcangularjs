﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DTO;
using Models;

namespace Repositories
{
    public class OccupancyProfile : Profile
    {

        public OccupancyProfile()
        {
            CreateMap<Models.Occupancy, DTO.Occupancy>()
                .ForMember(d => d.Landlord, opt => opt.MapFrom(s => s.Landlord))
                .ForMember(d => d.EstateAgent, opt => opt.MapFrom(s => s.EstateAgent))
                .ForMember(d => d.Occupiers, opt => opt.MapFrom(s => s.Occupiers))
                .ForMember(d => d.Occupiers, opt => opt.MapFrom(s => s.Occupiers))
                .ForMember(d => d.Vehicles, opt => opt.MapFrom(s => s.Vehicles))
                .ForMember(d => d.OccupierType, opt => opt.MapFrom(s => s.OccupierType));

            CreateMap<DTO.Occupancy, Models.Occupancy>();

            CreateMap<Models.Apartment, DTO.Apartment>()
                .ForMember(d => d.ApartmentType, opt => opt.MapFrom(s => s.ApartmentType));

            CreateMap<Models.Vehicle, DTO.Vehicle>();
                //.ForMember(d => d.Occupancy, opt => opt.MapFrom(s => s.Occupancy));

            CreateMap<DTO.Vehicle, Models.Vehicle>();

            //.ForMember(x => x., opt => opt.Ignore())

            CreateMap<DTO.Apartment, Models.Apartment>();

            CreateMap<DTO.ApartmentType, Models.ApartmentType>();
            CreateMap<Models.ApartmentType, DTO.ApartmentType>();

            CreateMap<DTO.OccupancyNote, Models.OccupancyNote>();
            CreateMap<Models.OccupancyNote, DTO.OccupancyNote>();

            CreateMap<DTO.Occupier, Models.Occupier>();
            CreateMap<Models.Occupier, DTO.Occupier>();

            CreateMap<DTO.OccupierType, Models.OccupierType>();
            CreateMap<Models.OccupierType, DTO.OccupierType>();

            CreateMap<DTO.Landlord, Models.Landlord>();
            CreateMap<Models.Landlord, DTO.Landlord>();

            CreateMap<DTO.EstateAgent, Models.EstateAgent>();
            CreateMap<Models.EstateAgent, DTO.EstateAgent>();
        }

    }
}

