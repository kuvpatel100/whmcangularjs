
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data.Entity;
using DTO;
using AutoMapper;

namespace Repositories
{
	public class ApartmentRepository : IApartmentRepository
    {
        private localdb db = new localdb();

        public ApartmentRepository()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<OccupancyProfile>();
            });
        }

        public List<DTO.Apartment> GetAll()
        {
            var list = new List<DTO.Apartment>();

            var apartments = from u in db.Apartments.Include("ApartmentType")
                             where u.Deleted == false
                        select u;

            foreach (var apartment in apartments)
            {
                DTO.Apartment model = Mapper.Map<Models.Apartment, DTO.Apartment>(apartment);
                list.Add(model);
            }

            return list;
        }

        public List<DTO.Apartment> GetApartmentList()
        {
            var list = new List<DTO.Apartment>();

            var occupancies = from o in db.Occupancies
                             where o.Deleted == false
                             select o;

            var apartments = from u in db.Apartments.Include("ApartmentType")
                             where u.Deleted == false
                             select u;

            foreach (var apartment in apartments)
            {
                // if an apartment does nto exists inthe occupancies i.e. is empty then add it to the list
                bool containsItem = occupancies.Any(item => item.ApartmentID == apartment.ID); 

                if (!containsItem)
                {
                    DTO.Apartment model = Mapper.Map<Models.Apartment, DTO.Apartment>(apartment);
                    list.Add(model);
                }
            }
            
            return list;
        }

        public List<DTO.Apartment> GetAll(int currentPage, int pageSize)
        {
            var list = new List<DTO.Apartment>();

            var apartments = db.Apartments.Include("ApartmentType")
                .OrderByDescending(u => u.ID)
                .Skip(currentPage)
                .Take(pageSize);

            foreach (var apartment in apartments)
            {
                DTO.Apartment model = Mapper.Map<Models.Apartment, DTO.Apartment>(apartment);
                list.Add(model);
            }

            return list;
        }

        public int ApartmentCount()
        {
            var count = db.Apartments.Count(x => x.Deleted == false);
            return count;
        }

        public DTO.Apartment Get(int ID)
        {
            var apartment = db.Apartments.Include("ApartmentType")
               .FirstOrDefault(u => u.ID == ID);

            DTO.Apartment apartmentDTO = Mapper.Map<Models.Apartment, DTO.Apartment>(apartment);

            return apartmentDTO;
        }

        public int Add(DTO.Apartment apartment)
        {
            // dto to poco
            Models.Apartment apartmentModel = Mapper.Map<DTO.Apartment, Models.Apartment>(apartment);
            apartmentModel.Deleted = false;
            apartmentModel.DateCreated = DateTime.Now;
            apartmentModel.DateUpdated = DateTime.Now;
            db.Apartments.Add(apartmentModel);
            Save();
            return apartmentModel.ID;
        }

        public bool Update(DTO.Apartment apartment)
        {
            // map dto to empty poco
            Models.Apartment ExistingApartment = new Models.Apartment();
            Mapper.Map<DTO.Apartment, Models.Apartment>(apartment, ExistingApartment);

            // get the existing record
            Models.Apartment ExistingApartment2 = db.Apartments.FirstOrDefault(u => u.ID == apartment.ID);
            
            // copy the new data poco to the existing record and save to db
            db.Entry(ExistingApartment2).CurrentValues.SetValues(ExistingApartment);
           
            ExistingApartment2.DateUpdated = DateTime.Now;
            db.Entry(ExistingApartment2).Property(x => x.DateCreated).IsModified = false;
            db.Entry(ExistingApartment2).Property(x => x.Deleted).IsModified = false;

            Save();
            return true;
        }

        public bool Delete(int ID)
        {
            bool result = true;
            try
            {
                db.Apartments.Remove(db.Apartments.Single(f => f.ID == ID));
                Save();
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public void Save()
        {
            db.SaveChanges();
        }

    }
}