
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace Repositories
{
    public interface IApartmentRepository
    {
		int Add(Apartment apartment);
        bool Delete(int ID);
        List<Apartment > GetAll();
        List<Apartment> GetApartmentList();
        List<Apartment > GetAll(int currentPage, int pageSize);
        Apartment  Get(int ID);
        void Save();
        bool Update(Apartment apartment);
        int ApartmentCount();
    }
}