using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace Repositories
{
    public interface IOccupierTypeRepository
    {
		int Add(OccupierType occupierType);
        bool Delete(int ID);
        List<OccupierType > GetAll();
        List<OccupierType > GetAll(int currentPage, int pageSize);
        OccupierType  Get(int ID);
        void Save();
        bool Update(OccupierType occupierType);
        int OccupierTypeCount();
    }
}