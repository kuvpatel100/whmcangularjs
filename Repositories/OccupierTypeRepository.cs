using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data.Entity;
using DTO;
using AutoMapper;

namespace Repositories
{
	public class OccupierTypeRepository : IOccupierTypeRepository
    {
        private localdb db = new localdb();

        public OccupierTypeRepository()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<OccupancyProfile>();
            });
        }

        public List<DTO.OccupierType> GetAll()
        {
            var list = new List<DTO.OccupierType>();

            var occupiertypes = from u in db.OccupierTypes
                        where u.Deleted == false
                        select u;

            foreach (var occupiertype in occupiertypes)
            {
                DTO.OccupierType model = Mapper.Map<Models.OccupierType, DTO.OccupierType>(occupiertype);
                list.Add(model);
            }

            return list;
        }

        public List<DTO.OccupierType> GetAll(int currentPage, int pageSize)
        {
            var list = new List<DTO.OccupierType>();

            var occupiertypes = db.OccupierTypes.Include("XXX").Include("XXX").Include("ApartmentType").Include("Occupiers").Include("Apartment").Include("OccupierType")
                .OrderByDescending(u => u.ID)
                .Skip(currentPage)
                .Take(pageSize);

            foreach (var occupiertype in occupiertypes)
            {
                DTO.OccupierType model = Mapper.Map<Models.OccupierType, DTO.OccupierType>(occupiertype);
                list.Add(model);
            }

            return list;
        }

        public int OccupierTypeCount()
        {
            var count = db.OccupierTypes.Count(x => x.Deleted == false);
            return count;
        }

        public DTO.OccupierType Get(int ID)
        {
            var occupiertype = db.OccupierTypes.Include("XXX").Include("XXX").Include("XXX").Include("Occupiers").Include("Apartment").Include("OccupierType")
               .FirstOrDefault(u => u.ID == ID);

            DTO.OccupierType occupiertypeDTO = Mapper.Map<Models.OccupierType, DTO.OccupierType>(occupiertype);

            return occupiertypeDTO;
        }

        public int Add(DTO.OccupierType occupiertype)
        {
            // dto to poco
            Models.OccupierType occupiertypeModel = Mapper.Map<DTO.OccupierType, Models.OccupierType>(occupiertype);
            occupiertypeModel.Deleted = false;
            db.OccupierTypes.Add(occupiertypeModel);
            Save();
            return occupiertypeModel.ID;
        }

        public bool Update(DTO.OccupierType occupiertype)
        {
            // map dto to empty poco
            Models.OccupierType ExistingOccupierType = new Models.OccupierType();
            Mapper.Map<DTO.OccupierType, Models.OccupierType>(occupiertype, ExistingOccupierType);

            // get the existing record
            Models.OccupierType ExistingOccupierType2 = db.OccupierTypes.FirstOrDefault(u => u.ID == occupiertype.ID);

            // copy the new data poco to the existing record and save to db
            db.Entry(ExistingOccupierType2).CurrentValues.SetValues(ExistingOccupierType);

            Save();
            return true;
        }

        public bool Delete(int ID)
        {
            bool result = true;
            try
            {
                db.OccupierTypes.Remove(db.OccupierTypes.Single(f => f.ID == ID));
                Save();
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public void Save()
        {
            db.SaveChanges();
        }

    }
}