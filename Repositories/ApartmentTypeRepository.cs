using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Data.Entity;
using DTO;
using AutoMapper;

namespace Repositories
{
	public class ApartmentTypeRepository : IApartmentTypeRepository
    {
        private localdb db = new localdb();

        public ApartmentTypeRepository()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<OccupancyProfile>();
            });
        }

        public List<DTO.ApartmentType> GetAll()
        {
            var list = new List<DTO.ApartmentType>();

            var apartmenttypes = from u in db.ApartmentTypes
                        where u.Deleted == false
                        select u;

            foreach (var apartmenttype in apartmenttypes)
            {
                DTO.ApartmentType model = Mapper.Map<Models.ApartmentType, DTO.ApartmentType>(apartmenttype);
                list.Add(model);
            }

            return list;
        }

        public List<DTO.ApartmentType> GetAll(int currentPage, int pageSize)
        {
            var list = new List<DTO.ApartmentType>();

            var apartmenttypes = db.ApartmentTypes.Include("XXX").Include("XXX").Include("ApartmentType").Include("Occupiers").Include("Apartment").Include("OccupierType")
                .OrderByDescending(u => u.ID)
                .Skip(currentPage)
                .Take(pageSize);

            foreach (var apartmenttype in apartmenttypes)
            {
                DTO.ApartmentType model = Mapper.Map<Models.ApartmentType, DTO.ApartmentType>(apartmenttype);
                list.Add(model);
            }

            return list;
        }

        public int ApartmentTypeCount()
        {
            var count = db.ApartmentTypes.Count(x => x.Deleted == false);
            return count;
        }

        public DTO.ApartmentType Get(int ID)
        {
            var apartmenttype = db.ApartmentTypes.Include("XXX").Include("XXX").Include("XXX").Include("Occupiers").Include("Apartment").Include("OccupierType")
               .FirstOrDefault(u => u.ID == ID);

            DTO.ApartmentType apartmenttypeDTO = Mapper.Map<Models.ApartmentType, DTO.ApartmentType>(apartmenttype);

            return apartmenttypeDTO;
        }

        public int Add(DTO.ApartmentType apartmenttype)
        {
            // dto to poco
            Models.ApartmentType apartmenttypeModel = Mapper.Map<DTO.ApartmentType, Models.ApartmentType>(apartmenttype);
            apartmenttypeModel.Deleted = false;
            db.ApartmentTypes.Add(apartmenttypeModel);
            Save();
            return apartmenttypeModel.ID;
        }

        public bool Update(DTO.ApartmentType apartmenttype)
        {
            // map dto to empty poco
            Models.ApartmentType ExistingApartmentType = new Models.ApartmentType();
            Mapper.Map<DTO.ApartmentType, Models.ApartmentType>(apartmenttype, ExistingApartmentType);

            // get the existing record
            Models.ApartmentType ExistingApartmentType2 = db.ApartmentTypes.FirstOrDefault(u => u.ID == apartmenttype.ID);

            // copy the new data poco to the existing record and save to db
            db.Entry(ExistingApartmentType2).CurrentValues.SetValues(ExistingApartmentType);

            Save();
            return true;
        }

        public bool Delete(int ID)
        {
            bool result = true;
            try
            {
                db.ApartmentTypes.Remove(db.ApartmentTypes.Single(f => f.ID == ID));
                Save();
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public void Save()
        {
            db.SaveChanges();
        }

    }
}