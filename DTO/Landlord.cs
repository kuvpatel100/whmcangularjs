using System;

namespace DTO
{
    public partial class Landlord
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }

        public string MobileOrLandline { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public bool Deleted { get; set; }

        public string Note { get; set; }
    }
}
