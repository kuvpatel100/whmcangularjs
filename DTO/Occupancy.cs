using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DTO
{
    public partial class Occupancy
    {
        public int ID { get; set; }

        public int ApartmentID { get; set; }

        public int? ParkingSpaceNo { get; set; }

        public int? ParkingSpaceNo2 { get; set; }

        public DateTime? DateOfOccupation { get; set; }

        public DateTime LeavingDate { get; set; }

        public int OccupierTypeID { get; set; }

        public int? LandlordID { get; set; }

        public int? EstateAgentID { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string MobileOrLandline { get; set; }

        public bool Deleted { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public virtual Apartment Apartment { get; set; }

        public virtual EstateAgent EstateAgent { get; set; }

        public virtual Landlord Landlord { get; set; }

        public virtual Collection<Occupier> Occupiers { get; set; }

        public virtual Collection<Vehicle> Vehicles { get; set; }

        public virtual OccupierType OccupierType { get; set; }

    }
}
