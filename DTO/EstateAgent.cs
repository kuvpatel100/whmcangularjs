using System;

namespace DTO
{    
    public partial class EstateAgent
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string ContactName { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public bool Deleted { get; set; }
    }
}
