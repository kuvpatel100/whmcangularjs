using System;

namespace DTO
{
    public partial class Apartment
    {
        public int ID { get; set; }

        public string ApartmentNo { get; set; }

        public int? FloorNo { get; set; }

        public int? NoOfBedrooms { get; set; }

        public int ApartmentTypeID { get; set; }

        public int? ParkingSpaceNo { get; set; }

        public int? ParkingSpaceNo2 { get; set; }

        public string Note { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public ApartmentType ApartmentType { get; set; }

    }
}
