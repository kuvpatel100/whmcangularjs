using System;

namespace DTO
{    
    public partial class OccupancyNote
    {
        public int ID { get; set; }

        public string Header { get; set; }

        public string Note { get; set; }

        public int OccupancyID { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public bool Deleted { get; set; }

    }
}
