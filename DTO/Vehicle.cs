﻿using System;

namespace DTO
{

    public partial class Vehicle
    {
        public int ID { get; set; }

        public string Registration { get; set; }

        public string MakeModel { get; set; }

        public string Colour { get; set; }

        public int? ParkingSpaceNo { get; set; }

        public int OccupancyID { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public bool Deleted { get; set; }

      //  public Occupancy Occupancy { get; set; }
    }
}
