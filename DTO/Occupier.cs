using System;

namespace DTO
{
    public partial class Occupier
    {
        public int ID { get; set; }

        public string Firstname { get; set; }

        public string Surname { get; set; }

        public bool IsPrimary { get; set; }
        
        public string Email { get; set; }
        
        public string Mobile { get; set; }
        
        public string MobileOrLandline { get; set; }

        public bool Deleted { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public int OccupancyID { get; set; }

        //public virtual Occupancy Occupancy { get; set; }
    }
}
