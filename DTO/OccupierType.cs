using System;

namespace DTO
{    
    public partial class OccupierType
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public bool Deleted { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }
    }
}
