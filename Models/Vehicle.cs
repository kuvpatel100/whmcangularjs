namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Vehicle")]
    public partial class Vehicle
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string Registration { get; set; }

        [StringLength(150)]
        public string MakeModel { get; set; }

        [StringLength(30)]
        public string Colour { get; set; }

        public int? ParkingSpaceNo { get; set; }

        public int OccupancyID { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public bool Deleted { get; set; }

        public virtual Occupancy Occupancy { get; set; }
    }
}
