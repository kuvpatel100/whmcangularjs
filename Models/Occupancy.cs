namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Occupancy")]
    public partial class Occupancy
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Occupancy()
        {
            Vehicles = new HashSet<Vehicle>();
            Occupiers = new HashSet<Occupier>();
        }

        public int ID { get; set; }

        public int ApartmentID { get; set; }

        public int? ParkingSpaceNo { get; set; }

        public int? ParkingSpaceNo2 { get; set; }

        public DateTime? DateOfOccupation { get; set; }

        public DateTime? LeavingDate { get; set; }

        public int OccupierTypeID { get; set; }

        public int? LandlordID { get; set; }

        public int? EstateAgentID { get; set; }

        [StringLength(150)]
        public string Email { get; set; }

        [StringLength(20)]
        public string Mobile { get; set; }

        [StringLength(20)]
        public string MobileOrLandline { get; set; }

        public bool Deleted { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public virtual Apartment Apartment { get; set; }

        public virtual EstateAgent EstateAgent { get; set; }

        public virtual Landlord Landlord { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vehicle> Vehicles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Occupier> Occupiers { get; set; }

        public virtual OccupierType OccupierType { get; set; }
    }
}
