namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Apartment")]
    public partial class Apartment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Apartment()
        {
            Occupancies = new HashSet<Occupancy>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string ApartmentNo { get; set; }

        public int? FloorNo { get; set; }

        public int? NoOfBedrooms { get; set; }

        public int ApartmentTypeID { get; set; }

        public int? ParkingSpaceNo { get; set; }

        public int? ParkingSpaceNo2 { get; set; }

        [Column(TypeName = "ntext")]
        public string Note { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public bool? Deleted { get; set; }

        public virtual ApartmentType ApartmentType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Occupancy> Occupancies { get; set; }
    }
}
