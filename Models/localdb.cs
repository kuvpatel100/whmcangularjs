namespace Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class localdb : DbContext
    {
        public localdb()
            : base("name=localdb")
        {
        }

        public virtual DbSet<Apartment> Apartments { get; set; }
        public virtual DbSet<ApartmentType> ApartmentTypes { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<EstateAgent> EstateAgents { get; set; }
        public virtual DbSet<Landlord> Landlords { get; set; }
        public virtual DbSet<Occupancy> Occupancies { get; set; }
        public virtual DbSet<OccupancyNote> OccupancyNotes { get; set; }
        public virtual DbSet<Occupier> Occupiers { get; set; }
        public virtual DbSet<OccupierType> OccupierTypes { get; set; }
        public virtual DbSet<Vehicle> Vehicles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Apartment>()
                .Property(e => e.ApartmentNo)
                .IsUnicode(false);

            modelBuilder.Entity<Apartment>()
                .HasMany(e => e.Occupancies)
                .WithRequired(e => e.Apartment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApartmentType>()
                .HasMany(e => e.Apartments)
                .WithRequired(e => e.ApartmentType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Occupancy>()
                .HasMany(e => e.Vehicles)
                .WithRequired(e => e.Occupancy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Occupancy>()
                .HasMany(e => e.Occupiers)
                .WithRequired(e => e.Occupancy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Occupier>()
                .Property(e => e.Firstname)
                .IsFixedLength();

            modelBuilder.Entity<Occupier>()
                .Property(e => e.Surname)
                .IsFixedLength();

            modelBuilder.Entity<OccupierType>()
                .HasMany(e => e.Occupancies)
                .WithRequired(e => e.OccupierType)
                .WillCascadeOnDelete(false);
        }
    }
}
