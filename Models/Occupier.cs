namespace Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Occupier")]
    public partial class Occupier
    {
        public int ID { get; set; }

        [StringLength(10)]
        public string Firstname { get; set; }

        [StringLength(10)]
        public string Surname { get; set; }

        public bool IsPrimary { get; set; }

        [StringLength(150)]
        public string Email { get; set; }

        [StringLength(20)]
        public string Mobile { get; set; }

        [StringLength(20)]
        public string MobileOrLandline { get; set; }

        public bool Deleted { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public int OccupancyID { get; set; }

        public virtual Occupancy Occupancy { get; set; }
    }
}
