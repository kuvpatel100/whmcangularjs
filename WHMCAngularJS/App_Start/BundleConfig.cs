﻿using System.Web;
using System.Web.Optimization;

namespace WHMCAngularJS
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                     "~/Scripts/bootstrap.js",
                    "~/Scripts/bootstrap-datepicker.min.js",
                    "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                       "~/Scripts/angular.js",
                       "~/Scripts/ui-bootstrap-tpls.js",
                       "~/Scripts/angular-sanitize.js",
                       "~/Scripts/angular-messages.js",
                       "~/Scripts/angular-route.min.js",
                       "~/Scripts/angular-touch.min.js",
                       "~/Scripts/angular-animate.min.js"
                       ));


            bundles.Add(new ScriptBundle("~/bundles/angularUIGrid").Include(
                      "~/Scripts/mg//package/angular-ui/grid.min.js",
                      "~/Scripts/export-ui-grid/csv.js",
                      "~/Scripts/export-ui-grid/pdfmake.js",
                      "~/Scripts/export-ui-grid/vfs_fonts.js"
                      ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                       "~/Content/bootstrap.css",
                       "~/Content/main.css",
                       "~/Content/bootstrap-datepicker.min.css"));
        }
    }
}
