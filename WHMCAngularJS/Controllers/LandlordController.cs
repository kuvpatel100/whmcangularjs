
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
using Repositories;
using WHMCAngularJS.ViewModel;

namespace WHMCAngularJs.Controllers
{
    public class LandlordController : Controller
    {
        Repositories.ILandlordRepository _landlordRepository = null;
        
        public LandlordController(ILandlordRepository landlordRepository)
        {
            _landlordRepository = landlordRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetByPage(int pageNumber, int pageSize)
        {
            List<DTO.Landlord>  landlordList = null;
            int recordsTotal = 0;
            try
            {
                 landlordList = _landlordRepository.GetAll(pageNumber, pageSize);
                recordsTotal = _landlordRepository. LandlordCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return Json(new
            {
                recordsTotal,
                landlordList
            });
        }

        public JsonResult GetAll()
        {
            List<Landlord> list = _landlordRepository.GetAll();
            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetByID(int id)
        {
             var landlord = _landlordRepository.Get(id);
             return Json(new { landlord = landlord, success = true, errorMessage = "" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            var result = _landlordRepository.Delete(id);

            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add( Landlord  landlord)
        {
            int id = _landlordRepository.Add( landlord);

            return Json(new
            {
                success = id > 0 ? true : false,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update( Landlord  landlord)
        {
            bool result = _landlordRepository.Update( landlord);
            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

    }

}