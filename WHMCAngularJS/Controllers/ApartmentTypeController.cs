using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
using Repositories;
using WHMCAngularJS.ViewModel;

namespace WHMCAngularJs.Controllers
{
    public class ApartmentTypeController : Controller
    {
        Repositories.IApartmentTypeRepository _apartmenttypeRepository = null;
        
        public ApartmentTypeController(IApartmentTypeRepository apartmenttypeRepository)
        {
            _apartmenttypeRepository = apartmenttypeRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetApartmentTypeByPage(int pageNumber, int pageSize)
        {
            List<DTO.ApartmentType>  apartmenttypeList = null;
            int recordsTotal = 0;
            try
            {
                 apartmenttypeList = _apartmenttypeRepository.GetAll(pageNumber, pageSize);
                recordsTotal = _apartmenttypeRepository. ApartmentTypeCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return Json(new
            {
                recordsTotal,
                 apartmenttypeList
            });
        }

        public JsonResult GetAll()
        {
            List<ApartmentType> list = _apartmenttypeRepository.GetAll();
            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Get(int id)
        {
             var apartmenttype = _apartmenttypeRepository.Get(id);
             return Json(new { apartmenttype = apartmenttype, success = true, errorMessage = "" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            var result = _apartmenttypeRepository.Delete(id);

            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(ApartmentType  apartmenttype)
        {
            int id = _apartmenttypeRepository.Add( apartmenttype);

            return Json(new
            {
                success = id > 0 ? true : false,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update(ApartmentType  apartmenttype)
        {
            bool result = _apartmenttypeRepository.Update( apartmenttype);
            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

    }

}