using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
using Repositories;
using WHMCAngularJS.ViewModel;

namespace WHMCAngularJs.Controllers
{
    public class OccupierTypeController : Controller
    {
        Repositories.IOccupierTypeRepository _occupiertypeRepository = null;
        
        public OccupierTypeController(IOccupierTypeRepository occupiertypeRepository)
        {
            _occupiertypeRepository = occupiertypeRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetByPage(int pageNumber, int pageSize)
        {
            List<DTO.OccupierType>  occupiertypeList = null;
            int recordsTotal = 0;
            try
            {
                occupiertypeList = _occupiertypeRepository.GetAll(pageNumber, pageSize);
                recordsTotal = _occupiertypeRepository. OccupierTypeCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return Json(new
            {
                recordsTotal,
                occupiertypeList
            });
        }

        public JsonResult GetAll()
        {
            List<OccupierType> list = _occupiertypeRepository.GetAll();
            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Get(int id)
        {
             var occupiertype = _occupiertypeRepository.Get(id);
             return Json(new { occupiertype = occupiertype, success = true, errorMessage = "" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            var result = _occupiertypeRepository.Delete(id);

            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add( OccupierType  occupiertype)
        {
            int id = _occupiertypeRepository.Add( occupiertype);

            return Json(new
            {
                success = id > 0 ? true : false,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update( OccupierType  occupiertype)
        {
            bool result = _occupiertypeRepository.Update( occupiertype);
            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

    }

}