using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
using Repositories;
using WHMCAngularJS.ViewModel;

namespace WHMCAngularJs.Controllers
{
    public class EstateAgentController : Controller
    {
        Repositories.IEstateAgentRepository _estateagentRepository = null;
        
        public EstateAgentController(IEstateAgentRepository estateagentRepository)
        {
            _estateagentRepository = estateagentRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetByPage(int pageNumber, int pageSize)
        {
            List<DTO.EstateAgent>  estateagentList = null;
            int recordsTotal = 0;
            try
            {
                 estateagentList = _estateagentRepository.GetAll(pageNumber, pageSize);
                recordsTotal = _estateagentRepository. EstateAgentCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return Json(new
            {
                recordsTotal,
                estateagentList
            });
        }

        public JsonResult GetAll()
        {
            List<EstateAgent> list = _estateagentRepository.GetAll();
            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetByID(int id)
        {
             var estateagent = _estateagentRepository.Get(id);
             return Json(new { estateagent = estateagent, success = true, errorMessage = "" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            var result = _estateagentRepository.Delete(id);

            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add( EstateAgent  estateagent)
        {
            int id = _estateagentRepository.Add( estateagent);

            return Json(new
            {
                success = id > 0 ? true : false,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update( EstateAgent  estateagent)
        {
            bool result = _estateagentRepository.Update( estateagent);
            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

    }

}