﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
using Repositories;
using WHMCAngularJS.ViewModel;

namespace WHMCAngularJs.Controllers
{
    public class OccupancyController : Controller
    {
        Repositories.IOccupancyRepository _occupancyRepository = null;
        Repositories.IOccupierRepository _occupierRepository = null;
        Repositories.IOccupancyNoteRepository _occupancyNoteRepository = null;

        public OccupancyController(IOccupancyRepository occupancyRepository, IOccupierRepository occupierRepository, IOccupancyNoteRepository occupancyNoteRepository)
        {
            _occupancyRepository = occupancyRepository;
            _occupierRepository = occupierRepository;
            _occupancyNoteRepository = occupancyNoteRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "WHMC Residential Management System";

            return View();
        }

        public JsonResult GetByPage(int pageNumber, int pageSize)
        {
            List<DTO.Occupancy> occupancyList = null;
            int recordsTotal = 0;
            try
            {
                occupancyList = _occupancyRepository.GetAll(pageNumber, pageSize);
                recordsTotal = _occupancyRepository.OccupancyCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return Json(new
            {
                recordsTotal,
                occupancyList
            });
        }

        public JsonResult GetAll()
        {
            List<Occupancy> list = _occupancyRepository.GetAll();
            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetByID(int id)
        {
            OccupancyViewModel model = new OccupancyViewModel();

            model.Occupancy = _occupancyRepository.Get(id);
            return Json(new { occupancy = model.Occupancy, success = true, errorMessage = "" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            OccupancyViewModel model = new OccupancyViewModel();

            var result = _occupancyRepository.Delete(id);

            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(Occupancy occupancy)
        {
            int id = _occupancyRepository.Add(occupancy);

            return Json(new
            {
                success = id > 0 ? true : false,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update(Occupancy occupancy)
        {
            bool result = _occupancyRepository.Update(occupancy);
            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

    }

}