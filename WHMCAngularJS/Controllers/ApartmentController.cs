using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
using Repositories;
using WHMCAngularJS.ViewModel;

namespace WHMCAngularJs.Controllers
{
    public class ApartmentController : Controller
    {
        Repositories.IApartmentRepository _apartmentRepository = null;
        
        public ApartmentController(IApartmentRepository apartmentRepository)
        {
            _apartmentRepository = apartmentRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetByPage(int pageNumber, int pageSize)
        {
            List<DTO.Apartment> apartmentList = null;
            int recordsTotal = 0;
            try
            {
                apartmentList = _apartmentRepository.GetAll(pageNumber, pageSize);
                recordsTotal = _apartmentRepository.ApartmentCount();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return Json(new
            {
                recordsTotal,
                apartmentList
            });
        }

        public JsonResult GetAll()
        {
            List<Apartment> list = _apartmentRepository.GetAll();
            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetApartmentList()
        {
            List<Apartment> list = _apartmentRepository.GetApartmentList();
            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Get(int id)
        {
            var apartment = _apartmentRepository.Get(id);
            return Json(new { apartment = apartment, success = true, errorMessage = "" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            var result = _apartmentRepository.Delete(id);

            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add( Apartment  apartment)
        {
            int id = _apartmentRepository.Add( apartment);

            return Json(new
            {
                success = id > 0 ? true : false,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update( Apartment  apartment)
        {
            bool result = _apartmentRepository.Update( apartment);
            return Json(new
            {
                success = result,
                errorMessage = ""
            }, JsonRequestBehavior.AllowGet);
        }

    }

}