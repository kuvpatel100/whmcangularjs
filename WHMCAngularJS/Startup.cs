﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WHMCAngularJS.Startup))]
namespace WHMCAngularJS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
