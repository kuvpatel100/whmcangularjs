﻿

app.controller("LandlordController", ['$http', '$scope', 'LandlordService', 'uiGridConstants',

    function ($http, $scope, LandlordService, uiGridConstants) {

        $scope.gridData;                        // grid date from service call is bound to the ui grid
        $scope.alerts = [];                     // alerts 
        $scope.saveFooterVisible = true;        // toggle footer buttons
        $scope.submitted = false;                // modal form submitted flag
        $scope.ph_numbr = /^0(\d ?){10}$/;
        $scope.eml_add = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;

        /********************************************************************  ALERTS AND MODAL EVENTS **************************************************************/

        $scope.addAlert = function (type, msg) {
            $scope.alerts.push({
                type: type,
                msg: msg
            })
        }

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        // reset validaiton when modal loads
        $('.modal').on('shown.bs.modal', function () {
            $scope.addForm.$setPristine();
        });

        // do cleanup when modal form is closed
        $('.modal').on('hidden.bs.modal', function () {
            $scope.alerts = [];                         // clear all alerts
            $scope.saveFooterVisible = true;            // set modal buttons to default
            $scope.submitted = false;
        });

        /********************************************************************  ANGULAR UI GRID ******************************************************/

        $scope.landlordGridOptions = {
            paginationPageSizes: [10, 15, 20],
            paginationPageSize: 10,
            useExternalPagination: false,
            useExternalSorting: false,
            enableFiltering: true,
            enableSorting: true,
            enableRowSelection: true,
            enableSelectAll: true,
            enableGridMenu: true,
            columnDefs: [
              { name: 'ID' },
              { name: 'Name' },
              { name: 'Mobile' },
              { name: "MobileOrLandline", displayName: "Mobile Or Landline", headerCellClass: $scope.highlightFilteredHeader },
              { name: 'Email' },
              { name: 'Address' },
              {
                  name: 'Actions',
                  enableFiltering: false,
                  enableSorting: false,
                  width: '15%',
                  enableColumnResizing: false,
                  cellTemplate: '<div style="padding-top: 4px; text-align: center;"><button type="button" class="btn btn-success btn-xs" ng-click="grid.appScope.editLandlordClicked(row.entity)" id="btnEditLandlord">Edit <span class="glyphicon glyphicon-edit"> </button>&nbsp&nbsp' +
                                '<button type="button" class="btn btn-danger btn-xs" ng-click="grid.appScope.deleteLandlordClicked(row.entity)" id="btnDeleteLandlord">Delete <span class="glyphicon glyphicon-remove"></span></button></div>'
              }
            ]
        };

        $scope.getLandlords = function () {
            LandlordService.getLandlords()
                .then(function (response) {
                    $scope.gridData = response.data;
                    $scope.landlordGridOptions.data = $scope.gridData;
                })
                .catch(function (response) {
                    $scope.status = 'Unable to load landlord data: ' + response.message;
                    console.log($scope.status);
                });
        };

        $scope.getLandlords();

        /********************************************************************  MODAL INIT ******************************************************/

        function initForm() {

            $scope.form =
            {
                ID: 0,
                Name: "",
                Mobile: "",
                MobileOrLandline: "",
                Email: "",
                Note: "",
                Address: ""
            };
        }

        /********************************************************************  MODAL EVENTS ******************************************************/

        $scope.addLandlordClicked = function () {
            initForm();
            $scope.saveFooterVisible = true;
            $("#addLandlordModal").modal();
        }

        $scope.editLandlordClicked = function (model) {
            var id = model.ID;
            LandlordService.getLandlord(id)
               .then(function (response) {
                   $scope.form = response.data.landlord;
                   $("#editLandlordModal").modal();
               })
               .catch(function (response) {
                   $scope.status = 'Unable to edit landlord ' + response.message;
                   console.log($scope.status);
               });
        }

        $scope.deleteLandlordClicked = function (model) {
            $scope.DeleteLandlordID = model.ID;
            $("#confirmLandlordModal").modal();
        }

        $scope.deleteLandlordYesClicked = function () {

            var id = $scope.DeleteLandlordID;

            LandlordService.deleteLandlord(id)
               .then(function (response) {

                   if (response.data.success === true) {
                       $scope.addAlert('success', 'Landlord was deleted successfully');
                       $scope.getLandlords();
                   }
                   else {
                       $scope.addAlert('danger', 'Unable to delete Landlord details');
                   }
               })
               .catch(function (response) {
                   $scope.status = 'Unable to delete landlord ' + response.message;
               });
        }

        /********************************************************************  MODAL CRUD ******************************************************/

        $scope.updateLandlord = function () {

            LandlordService.updateLandlord($scope.form)
                .then(function (response) {
                    if (response.data.success === true) {
                        $scope.addAlert('success', 'Landlord was updated successfully');
                        $scope.saveFooterVisible = false;
                        $scope.getLandlords();
                    }
                    else {
                        $scope.addAlert('danger', 'Unable to update Landlord details');
                    }
                })
                .catch(function (response) {
                    $scope.status = 'Unable to update landlord ' + response.message;
                });
        }

        $scope.addLandlord = function () {
            $scope.submitted = true;
            $scope.alerts = [];

            if ($scope.addForm.$valid) {
                LandlordService.addLandlord($scope.form)
              .then(function (response) {
                  if (response.data.success === true) {
                      $scope.submitted = false;
                      $scope.addAlert('success', 'Landlord was added successfully');
                      $scope.saveFooterVisible = false;
                      $scope.getLandlords();
                  }
                  else {
                      $scope.addAlert('danger', 'Unable to add Landlord details');
                  }
              })
              .catch(function (response) {
                  $scope.status = 'Unable to add landlord ' + response.message;
                  console.log($scope.status);
              });

            }
            else {
                $scope.addAlert('danger', 'Please correct errors');
            }
        }
    }
]);
