﻿

app.controller("ApartmentController", ['$http', '$scope', 'ApartmentService', 'ApartmentTypeService', 'uiGridConstants',

    function ($http, $scope, ApartmentService, ApartmentTypeService, uiGridConstants) {

        $scope.gridData;                        // grid date from service call is bound to the ui grid
        $scope.alerts = [];                     // alerts 
        $scope.saveFooterVisible = true;        // toggle footer buttons
        $scope.submitted = false;                // modal form submitted flag
        $scope.ph_numbr = /^\+?\d{10}$/;
        $scope.eml_add = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;

        /********************************************************************  ALERTS AND MODAL EVENTS **************************************************************/

        $scope.addAlert = function (type, msg) {
            $scope.alerts.push({
                type: type,
                msg: msg
            })
        }
        
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        // reset validaiton when modal loads
        $('.modal').on('shown.bs.modal', function () {
            $scope.addForm.$setPristine();
        });

        // do cleanup when modal form is closed
        $('.modal').on('hidden.bs.modal', function () {
            $scope.alerts = [];                         // clear all alerts
            $scope.saveFooterVisible = true;            // set modal buttons to default
            $scope.submitted = false;
        });

        /********************************************************************  ANGULAR UI GRID ******************************************************/

        $scope.apartmentGridOptions = {
            paginationPageSizes: [10, 15, 20],
            paginationPageSize: 10,
            useExternalPagination: false,
            useExternalSorting: false,
            enableFiltering: true,
            enableSorting: true,
            enableRowSelection: true,
            enableSelectAll: true,
            enableGridMenu: true,
            columnDefs: [
              { name: 'ID' },
              { name: 'ApartmentNo', displayName: 'Apt No' },
              { name: 'ParkingSpaceNo', displayName: 'Parking Space No 2'  },
              { name: 'ParkingSpaceNo2', displayName: 'Parking Space No 2' },
              { name: 'FloorNo', displayName: 'Floor No' },
              { name: 'NoOfBedrooms', displayName: 'No Of Bedrooms' },
              { name: 'ApartmentType.Name', displayName: 'Type' },
              { name: 'Note' },
              {
                  name: 'Actions',
                  enableFiltering: false,
                  enableSorting: false,
                  width: '15%',
                  enableColumnResizing: false,
                  cellTemplate: '<div style="padding-top: 4px; text-align: center;"><button type="button" class="btn btn-success btn-xs" ng-click="grid.appScope.editApartmentClicked(row.entity)" id="btnEditApartment">Edit <span class="glyphicon glyphicon-edit"> </button>&nbsp&nbsp' +
                                '<button type="button" class="btn btn-danger btn-xs" ng-click="grid.appScope.deleteApartmentClicked(row.entity)" id="btnDeleteApartment">Delete <span class="glyphicon glyphicon-remove"></span></button></div>'
              }
            ]
        };
      
        $scope.getApartments = function () {
            ApartmentService.getApartments()
                .then(function (response) {
                    $scope.gridData = response.data;
                    $scope.apartmentGridOptions.data = $scope.gridData;
                })
                .catch(function (response) {
                    $scope.status = 'Unable to load apartment data: ' + response.message;
                    console.log($scope.status);
                });
        };

        $scope.getApartments();

        /********************************************************************  MODAL INIT ******************************************************/

        function initForm() {
            $scope.selectedApartmentType = 'Apartment Type';
            $scope.selectedApartmentTypeId = -1;

            $scope.form =
            {
                ID: 0,
                ApartmentNo: "",
                FloorNo: "",
                NoOfBedrooms: "",
                ParkingSpaceNo: "",
                Note: ''
            };
        }
     
        $scope.getApartmentTypes = function () {

            ApartmentTypeService.getApartmentTypes()
                .then(function (response) {
                    $scope.apartmenttypes = response.data;
                })
                .catch(function (response) {
                    $scope.status = 'Unable to load apartment type data: ' + response.message;
                    console.log($scope.status);
                });
        }

        $scope.getApartmentTypes();

        /********************************************************************  MODAL EVENTS ******************************************************/
       
        $scope.apartmenttypeselected = function (item) {
            $scope.selectedApartmentType = item.Name;
            $scope.selectedApartmentTypeId = item.ID;
        }

        $scope.addApartmentClicked = function () {
            initForm();
            $scope.saveFooterVisible = true;
            $("#addApartmentModal").modal();
        }

        $scope.editApartmentClicked = function (model) {
            var id = model.ID;
            ApartmentService.getApartment(id)
               .then(function (response) {
                   $scope.formEdit = response.data.apartment;
                   $scope.selectedApartmentTypeId = response.data.apartment.ApartmentTypeID;
                   $scope.selectedApartmentType = response.data.apartment.ApartmentType.Name;

                   $("#editApartmentModal").modal();
               })
               .catch(function (response) {
                   $scope.status = 'Unable to edit apartment ' + response.message;
                   console.log($scope.status);
               });
        }

        $scope.deleteApartmentClicked = function (model) {
            $scope.DeleteApartmentID = model.ID;
            $("#confirmApartmentModal").modal();
        }

        $scope.deleteApartmentYesClicked = function () {

            var id = $scope.DeleteApartmentID;

            ApartmentService.deleteApartment(id)
               .then(function (response) {

                   if (response.data.success === true) {
                       $scope.addAlert('success', 'Apartment was deleted successfully');
                       $scope.getApartments();
                   }
                   else {
                       $scope.addAlert('danger', 'Unable to delete Apartment details');
                   }
               })
               .catch(function (response) {
                   $scope.status = 'Unable to delete apartment ' + response.message;
               });
        }

        /********************************************************************  MODAL CRUD ******************************************************/

        $scope.updateApartment = function () {
            $scope.formEdit.ApartmentTypeID = $scope.selectedApartmentTypeId;

            if ($scope.selectedApartmentTypeId == -1) {
                $scope.editForm.dropdownApartmentType.$setValidity('required', false);
            }

            if ($scope.editForm.$valid) {
                  ApartmentService.updateApartment($scope.formEdit)
                 .then(function (response) {
                     if (response.data.success === true) {
                         $scope.submitted = false;
                         $scope.addAlert('success', 'Apartment was added successfully');
                         $scope.saveFooterVisible = false;
                         $scope.getApartments();
                     }
                     else {
                         $scope.addAlert('danger', 'Unable to add Apartment details');
                     }
                 })
                 .catch(function (response) {
                     $scope.status = 'Unable to add apartment ' + response.message;
                     console.log($scope.status);
                 });
            }

        }

        $scope.addApartment = function () {
            $scope.form.ApartmentTypeID = $scope.selectedApartmentTypeId;
            $scope.submitted = true;
            $scope.alerts = [];

            // manually set the the field as invalid if a drop down is not selected
            if ($scope.selectedApartmentTypeId == -1)
            {
                $scope.addForm.dropdownApartmentType.$setValidity('required', false);
            }

            if ($scope.addForm.$valid) {
                ApartmentService.addApartment($scope.form)
              .then(function (response) {
                  if (response.data.success === true) {
                      $scope.submitted = false;
                      $scope.addAlert('success', 'Apartment was added successfully');
                      $scope.saveFooterVisible = false;
                      $scope.getApartments();
                  }
                  else {
                      $scope.addAlert('danger', 'Unable to add Apartment details');
                  }
              })
              .catch(function (response) {
                  $scope.status = 'Unable to add apartment ' + response.message;
                  console.log($scope.status);
              });

            }
            else
            {
                $scope.addAlert('danger', 'Please correct errors');
            }
        }
    }
]);
