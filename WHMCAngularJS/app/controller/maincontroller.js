﻿

app.controller("OccupancyController", ['$http', '$scope', '$rootScope', 'OccupancyService', 'ApartmentService', 'uiGridConstants', 'ApartmentTypeService', 'OccupierTypeService', 'EstateAgentService', 'LandlordService', 'OccupancyNoteTypeService',

function ($http, $scope, $rootScope, OccupancyService, ApartmentService, uiGridConstants, ApartmentTypeService, OccupierTypeService, EstateAgentService, LandlordService, OccupancyNoteTypeService) {

        $scope.gridOptions = [];
        $scope.gridData;                         // grid date from service call is bound to the ui grid
        $scope.addOccupancyGridOptions = [];
        $scope.addOccupancyGridData;             // grid date from service call is bound to the ui grid
        $scope.alerts = [];                      // alerts 
        $scope.saveFooterVisible = true;         // toggle footer buttons
        $scope.submitted = false;                // modal form submitted flag
        $scope.ph_numbr = /^\+?\d{10}$/;
        $scope.eml_add = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
        $scope.showDoneButton = false;
        /********************************************************************  ALERTS AND MODAL EVENTS ***********************************************/

        $scope.addAlert = function (type, msg) {
            $scope.alerts.push({
                type: type,
                msg: msg
            })
        }

        $scope.getPenthouseNumbers = function () {
            return $rootScope.PenthouseNumbers;
        };

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        // reset validaiton when modal loads
        $('.modal').on('shown.bs.modal', function () {
       //     $scope.addForm.$setPristine();
        });

        // do cleanup when modal form is closed
        $('.modal').on('hidden.bs.modal', function () {
            $scope.alerts = [];                         // clear all alerts
            $scope.saveFooterVisible = true;            // set modal buttons to default
            $scope.submitted = false;
        });

        /********************************************************************  ANGULAR UI GRID ******************************************************/

        $scope.pagination = {
            paginationPageSizes: [10, 25, 50, 75, 100, "All"],
            ddlpageSize: 15,
            pageNumber: 1,
            pageSize: 25,
            totalItems: 0,

            getTotalPages: function () {
                return Math.ceil(this.totalItems / this.pageSize);
            },
            pageSizeChange: function () {
                if (this.ddlpageSize == "All")
                    this.pageSize = $scope.pagination.totalItems;
                else
                    this.pageSize = this.ddlpageSize

                this.pageNumber = 1
                $scope.GetOccupancies();
            },
            firstPage: function () {
                if (this.pageNumber > 1) {
                    this.pageNumber = 1
                    $scope.GetOccupancies();
                }
            },
            nextPage: function () {
                if (this.pageNumber < this.getTotalPages()) {
                    this.pageNumber++;
                    $scope.GetOccupancies();
                }
            },
            previousPage: function () {
                if (this.pageNumber > 1) {
                    this.pageNumber--;
                    $scope.GetOccupancies();
                }
            },
            lastPage: function () {
                if (this.pageNumber >= 1) {
                    this.pageNumber = this.getTotalPages();
                    $scope.GetOccupancies();
                }
            }
        };

        $scope.getCellFormatDate = function (model) {
            // JSON date to javascript date
            var date = model
            var result = '';

            if (model !== null) {
                var input = new Date(date.match(/\d+/)[0] * 1);

                var dd = input.getDate();
                var mm = input.getMonth() + 1;
                var yyyy = input.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }

                result = dd + '/' + mm + '/' + yyyy;
                if (result === '01/01/3939') {
                    result = '';
                }
            }

            return result;
        }

        $scope.getMobile = function (occupiers) {
            var mobile = '';
            if (occupiers !== null && occupiers !== undefined) {
                if (occupiers.length > 0 ) {
                    mobile = occupiers[0].Mobile;
                }
            }
            return mobile;
        };

        $scope.getEmail = function (occupiers) {
            var email = '';
            if (occupiers !== null && occupiers !== undefined) {
                if (occupiers.length > 0) {
                    email = occupiers[0].Email;
                }
            }
            return email;
        };

        $scope.getFullname = function (occupiers) {
            var name = '';
            if (occupiers !== null && occupiers !== undefined) {
                if (occupiers.length > 0) {
                    name = occupiers[0].Firstname + ' ' + occupiers[0].Surname;
                }
            }
            return name;
        };

        $scope.getRegNo = function (vehicles) {
            var reg = '';
            if (vehicles !== null && vehicles !== undefined) {
                if (vehicles.length > 0) {
                    reg = vehicles[0].Registration.toUpperCase();
                    if (vehicles.length > 1) {
                        reg = reg + ', ' + vehicles[1].Registration.toUpperCase();
                    }
                }
            }
            return reg;
        };

        $scope.getParkingSpace = function (vehicles) {
            var space = '';
            if (vehicles !== null && vehicles !== undefined) {
                if (vehicles.length > 0) {
                    space = vehicles[0].ParkingSpaceNo;
                    if (vehicles.length > 1) {
                        space = space + ',' + vehicles[1].ParkingSpaceNo;
                    }
                }
            }
            return space;
        };

        $scope.getMakeModel = function (vehicles) {
            var model = '';
            if (vehicles !== null && vehicles !== undefined) {
                if (vehicles.length > 0) {
                    model = vehicles[0].MakeModel;
                }
            }
            return model;
        };

        //ui-Grid Call
        $scope.GetOccupancies = function () {
            $scope.loaderMore = true;
            $scope.lblMessage = 'loading please wait....!';
            $scope.result = "color-green";

            $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                if (col.filters[0].term) {
                    return 'header-filtered';
                } else {
                    return '';
                }
            };
            $scope.gridOptions = {
                useExternalPagination: true,
                useExternalSorting: false,
                enableFiltering: true,
                enableSorting: true,
                enableRowSelection: true,
                enableSelectAll: true,
                enableGridMenu: true,
                rowHeight: 35,
               
                columnDefs: [
                    { name: "ID", displayName: "ID", headerCellClass: $scope.highlightFilteredHeader },
                    { name: "Apartment.ApartmentNo", displayName: "Apt No", headerCellClass: $scope.highlightFilteredHeader },
                    {
                        name: "Fullanme",
                        displayName: "Fullname",
                        headerCellClass: $scope.highlightFilteredHeader,
                        cellTemplate: '<div style="padding-top:5px; padding-left: 3px">{{grid.appScope.getFullname(row.entity.Occupiers)}}</div>',
                    },
                   
                    {
                        name: "Mobile",
                        displayName: "Mobile",
                        enableSorting: true,
                        headerCellClass: $scope.highlightFilteredHeader,
                        cellTemplate: '<div style="padding-top:5px; padding-left: 3px">{{grid.appScope.getMobile(row.entity.Occupiers)}}</div>',
                    },
                    {
                        name: "Email",
                        displayName: "Email",
                        enableSorting: true,
                        headerCellClass: $scope.highlightFilteredHeader,
                        cellTemplate: '<div style="padding-top:5px; padding-left: 3px">{{grid.appScope.getEmail(row.entity.Occupiers)}}</div>',
                    },
                    {
                        name: "RegNo",
                        displayName: "RegNo",
                        enableSorting: true,
                        headerCellClass: $scope.highlightFilteredHeader,
                        cellTemplate: '<div style="padding-top:5px; padding-left: 3px">{{grid.appScope.getRegNo(row.entity.Vehicles)}}</div>',
                    },
                    {
                        name: "ParkingSpaceNo",
                        displayName: "ParkingSpaceNo",
                        enableSorting: true,
                        headerCellClass: $scope.highlightFilteredHeader,
                        cellTemplate: '<div style="padding-top:5px; padding-left: 3px">{{grid.appScope.getParkingSpace(row.entity.Vehicles)}}</div>',
                    },
                    {
                        name: "MakeModel",
                        displayName: "MakeModel",
                        enableSorting: true,
                        headerCellClass: $scope.highlightFilteredHeader,
                        cellTemplate: '<div style="padding-top:5px; padding-left: 3px">{{grid.appScope.getMakeModel(row.entity.Vehicles)}}</div>',
                    },
                    {
                        name: "DateOfOccupation",
                        displayName: "Date Of Occupation",
                        enableSorting: true,
                     //   width: '10%',
                        cellFilter: 'date:"short"',
                        headerCellClass: $scope.highlightFilteredHeader,
                        cellTemplate: '<div style="padding-top:5px; padding-left: 3px">{{grid.appScope.getCellFormatDate(row.entity.DateOfOccupation)}}</div>',
                    },
                    { name: "OccupierType.Name", displayName: "Occupier Type", title: "Occupier Type", headerCellClass: $scope.highlightFilteredHeader },
                    {
                        name: "LeavingDate",
                        displayName: "Leaving Date",
                        enableSorting: true,
                        cellFilter: 'date:"short"',
                        headerCellClass: $scope.highlightFilteredHeader,
                        cellTemplate: '<div style="padding-top:5px; padding-left: 3px">{{grid.appScope.getCellFormatDate(row.entity.LeavingDate)}}</div>',
                    },

                    {
                        name: 'Actions',
                        enableFiltering: false,
                        enableSorting: false,
                        
                        enableColumnResizing: false,
                        cellTemplate: '<div style="padding-top: 4px; text-align: center;"><button type="button" class="btn btn-success btn-xs" ng-click="grid.appScope.editOccupancyClicked(row.entity)" id="btnEditOccupancy">Edit <span class="fa fa-edit"> </button>&nbsp&nbsp' +
                                      '<button type="button" class="btn btn-danger btn-xs" ng-click="grid.appScope.deleteOccupancyClicked(row.entity)" id="btnDeleteOccupancy">Delete <span class="glyphicon glyphicon-remove"></span></button></div>'
                    }
                ],
                exporterAllDataFn: function () {
                    return getPage(1, $scope.gridOptions.totalItems, paginationOptions.sort)
                    .then(function () {
                        $scope.gridOptions.useExternalPagination = false;
                        $scope.gridOptions.useExternalSorting = false;
                        getPage = null;
                    });
                },
            };
            var NextPage = (($scope.pagination.pageNumber - 1) * $scope.pagination.pageSize);
            var NextPageSize = $scope.pagination.pageSize;

            OccupancyService.getOccupancies(NextPage, NextPageSize)
                .then(function (response) {
                    $scope.gridData = response.data;
                    $scope.pagination.totalItems = $scope.gridData.recordsTotal;
                    $scope.gridOptions.data = $scope.gridData.occupancyList;
                    $scope.loaderMore = false;
                })
             .catch(function (response, status, error) {
                 $scope.status = 'Unable to add occupancy ' + response.message;
                 console.log($scope.status);
             });

        }


        /********************************************************************  Add Occupancy Grid ***********************************************/

        $scope.addOccupancyGridOptions = {
            paginationPageSizes: [10, 15, 20],
            paginationPageSize: 10,
            useExternalPagination: true,
            useExternalSorting: false,
            enableFiltering: false,
            enableSorting: true,
            enableRowSelection: true,
            enableSelectAll: true,
            enableGridMenu: false,
            columnDefs: [
              { name: 'ID', visible: false },
              { name: 'Firstname',  },
              { name: 'Surname' },
              { name: 'Mobile' },
              { name: "MobileOrLandline", displayName: "Mobile Or Landline", headerCellClass: $scope.highlightFilteredHeader },
              { name: 'Email' },
              { name: 'IsPrimary', displayName: 'Is Primary' },
              {
                  name: 'Actions',
                  enableFiltering: false,
                  enableSorting: false,
                  width: '15%',
                  enableColumnResizing: false,
                  cellTemplate: '<div style="padding-top: 4px; text-align: center;"><button type="button" class="btn btn-success btn-xs" ng-click="grid.appScope.editAddOccupierClicked(row.entity)" id="btnEditAddFormOccupier">Edit <span class="glyphicon glyphicon-edit"> </button>&nbsp&nbsp' +
                                '<button type="button" class="btn btn-danger btn-xs" ng-click="grid.appScope.deleteAddOccupierClicked(row.entity)" id="btnDeleteAddOccupier">Delete <span class="glyphicon glyphicon-remove"></span></button></div>'
              }
            ]
        };

        $scope.addOccupancyGridNoteOptions = {
            paginationPageSizes: [10, 15, 20],
            paginationPageSize: 10,
            useExternalPagination: true,
            useExternalSorting: false,
            enableFiltering: false,
            enableSorting: true,
            enableRowSelection: true,
            enableSelectAll: true,
            enableGridMenu: false,
            columnDefs: [
              { name: 'ID', visible: false },
              { name: 'Header', },
              { name: 'Note' },
              { name: 'DateUpdated', displayName: 'Date Updated' },
              {
                  name: 'Actions',
                  enableFiltering: false,
                  enableSorting: false,
                  width: '15%',
                  enableColumnResizing: false,
                  cellTemplate: '<div style="padding-top: 4px; text-align: center;"><button type="button" class="btn btn-success btn-xs" ng-click="grid.appScope.editAddNoteClicked(row.entity)" id="btnAddNote">Edit <span class="glyphicon glyphicon-edit"> </button>&nbsp&nbsp' +
                                '<button type="button" class="btn btn-danger btn-xs" ng-click="grid.appScope.deleteNoteClicked(row.entity)" id="btnDeleteNote">Delete <span class="glyphicon glyphicon-remove"></span></button></div>'
              }
            ]
        };

        /********************************************************************  Get Lookup Data **************************************************/
        
        $scope.getApartments = function () {

            ApartmentService.getApartmentList()
                .then(function (response) {
                    $scope.apartments = response.data;
                })
                .catch(function (response) {
                    $scope.status = 'Unable to load apartment data: ' + response.message;
                    console.log($scope.status);
                });
        }

        $scope.getAllApartments = function () {

            ApartmentService.getApartments()
                .then(function (response) {
                    $scope.apartments = response.data;
                })
                .catch(function (response) {
                    $scope.status = 'Unable to load apartment data: ' + response.message;
                    console.log($scope.status);
                });
        }

        $scope.getApartmentTypes = function () {

            ApartmentTypeService.getApartmentTypes()
                .then(function (response) {
                    $scope.apartmenttypes = response.data;
                })
                .catch(function (response) {
                    $scope.status = 'Unable to load apartment type data: ' + response.message;
                    console.log($scope.status);
                });
        }

        $scope.getOccupierTypes = function () {

            OccupierTypeService.getOccupierTypes()
                .then(function (response) {
                    $scope.occupiertypes = response.data;
                })
                .catch(function (response) {
                    $scope.status = 'Unable to load occupier type data: ' + response.message;
                    console.log($scope.status);
                });
        }

        $scope.getEstateAgents = function () {

            EstateAgentService.getEstateAgents()
                .then(function (response) {
                    var obj = response.data;
                    obj.unshift({ "ID": "0", "Name": "Add New..." });
                    $scope.estateagents = obj;
                })
                .catch(function (response) {
                    $scope.status = 'Unable to load estate agents: ' + response.message;
                    console.log($scope.status);
                });
        }

        $scope.getLandlords = function () {

            LandlordService.getLandlords()
                .then(function (response) {
                    var obj = response.data;
                    obj.unshift({ "ID": "0", "Name": "Add New..." });
                    $scope.landlords = obj;

                })
                .catch(function (response) {
                    $scope.status = 'Unable to load landlords: ' + response.message;
                    console.log($scope.status);
                });
        }

        $scope.getOccupierStatus = function () {

            $scope.OccupierStatus = OccupierTypeService.getOccupierStatus();
        }

        /********************************************************************  DEFAULT LOAD ****************************************************/
        
        $scope.GetOccupancies();
        $scope.getApartments();
        $scope.getApartmentTypes();
        $scope.getOccupierTypes();
        $scope.getEstateAgents();
        $scope.getLandlords();
        $scope.getOccupierStatus();

        $('#colorselector_1').colorselector({
            callback: function (value, color, title) {
                $scope.vehicle1.Colour = title;
                $scope.ColourHex1 = color;
                $scope.$apply();
            }
        });

        $('#colorselector_2').colorselector({
            callback: function (value, color, title) {
                $scope.vehicle1.Colour = title;
                $scope.ColourHex2 = color;
              //  $scope.$apply();
            }
        });

        /********************************************************************  MODAL INIT ******************************************************/

        function initForm() {

            $scope.selectedApartment = 'Select Apartment...';
            $scope.selectedApartmentId = -1;

            $scope.selectedApartmentType = 'Apartment Type';
            $scope.selectedApartmentTypeId = -1;

            $scope.selectedOccupierType = 'Select Occupier Type...';
            $scope.selectedOccupierTypeId = -1;

            $scope.selectedOccupierStatus = 'Select Occupier Status...';
            $scope.selectedOccupierStatusId = -1;

            $scope.selectedEstateAgent = 'Select...';
            $scope.selectedEstateAgentId = -1
            $scope.addNewEstateAgent = false;
            $scope.showEstateAgentFields = false;
            $scope.newestateAgent;
            $scope.estateAgentDropDownVisible = true;

            $scope.selectedLandlord = 'Select...';
            $scope.selectedLandlordId = -1
            $scope.addNewLandlord = false;
            $scope.showLandlordFields = false;
            $scope.newLandlord;
            $scope.landlordDropDownVisible = true;
            $scope.showDoneButton = false;

            $scope.occupier =  { ID: -1, Firstname: '', Surname: '', IsPrimary: false, OccupancyID: 0, Email: '', Mobile: '', MobileOrLandline: '' };
            $scope.vehicle1 = { ID: -1, Registration: '', MakeModel: '', Colour: '', OccupancyID: 0, ParkingSpaceNo: 0 };
            $scope.vehicle2 = { ID: -1, Registration: '', MakeModel: '', Colour: '', OccupancyID: 0, ParkingSpaceNo: 0 };

            $scope.ColourHex1 = '';
            $scope.ColourHex2 = '';

            $scope.form =
            {
                ID: 0,
                ApartmentID : 0,
                EstateAgentID: 0,
                LandlordID: 0,
                OccupierTypeID : 0,
                DateOfOccupation: '',
                ParkingSpaceNo: '',
                ParkingSpaceNo2: 0,
                LeavingDate: '',
                estateAgent: { ID: -1, Name: '', ContactName: '', PhoneNumber: '', Email: '', Address: '' },
                landlord: { ID: -1, Name: '', Mobile: '', MobileOrLandline: '', Email: '', Address: '', Note: '' },
                Apartment: { ID: -1, ApartmentNo: '', FloorNo: '', NoOfBedrooms: '', ApartmentTypeID: 0, ParkingSpaceNo: 0, ParkingSpaceNo2: 0, Note: '' },
                Occupiers: [],
                Vehicles: []
            };

            $scope.addOccupancyGridData = [];
            $scope.addOccupancyGridOptions.data = [];
        }
         
        function SetTestData() {
            $scope.form.EstateAgentID = 1;
            $scope.form.LandlordID = 1;
            $scope.form.OccupierTypeID = 1;
        }

        function GetOccupancies() {

            GetOccupancyService.getGetOccupancies()
                .then(function (response) {
                   // $scope.occupancies = response.data;

                    $scope.gridData = response.data;
                    $scope.pagination.totalItems = $scope.gridData.recordsTotal;
                    $scope.gridOptions.data = $scope.gridData.occupancyList;
                    $scope.loaderMore = false;

                })
                .catch(function (response) {
                    $scope.status = 'Unable to load occupancy data: ' + response.message;
                    console.log($scope.status);
                });
        }

        /********************************************************************  MODAL EVENTS ******************************************************/

        $scope.dropboxitemselected = function (item) {
            $scope.selectedCategory = item.Name;
            $scope.selectedCategoryId = item.ID;
        }

        $scope.apartmentitemselected = function (item) {
           // $scope.form.parkingSpaceNo2 = 0;
            $scope.selectedApartment = item.ApartmentNo;
            $scope.selectedApartmentId = item.ID;

            $scope.vehicle1.ParkingSpaceNo = item.ParkingSpaceNo;
            $scope.vehicle2.ParkingSpaceNo = item.ParkingSpaceNo2;

            //$scope.form.parkingSpaceNo = item.ParkingSpaceNo;
            // $scope.form.parkingSpaceNo2 = item.ParkingSpaceNo2;
            // $scope.form.floorNo = item.FloorNo;
            // $scope.form.noOfBedrooms = item.NoOfBedrooms;

            $scope.selectedApartmentType = item.ApartmentType.Name;
            $scope.selectedApartmentTypeId = item.ApartmentType.ID;
            $scope.apartment = item;
        }

        $scope.apartmenttypeselected = function (item) {
            $scope.selectedApartmentType = item.Name;
            $scope.selectedApartmentTypeId = item.ID;
        }

        $scope.occupiertypeselected = function (item) {
            $scope.selectedOccupierType = item.Name;
            $scope.selectedOccupierTypeId = item.ID;
        }

        $scope.estateagentselected = function (item) {
            $scope.selectedEstateAgent = item.Name;
            $scope.selectedEstateAgentId = item.ID;
            $scope.showEstateAgentFields = true;
            if (item.ID == 0) {
                $scope.newestateAgent =
                    {
                        ID: -2,
                        Name: '',
                        ContactName: '',
                        PhoneNumber: '',
                        Email: '',
                        Address: ''
                    };
                $scope.form.estateAgent = $scope.newestateAgent;
                $scope.addNewEstateAgent = true;
                $scope.estateAgentDropDownVisible = false;
            }
            else {
                $scope.addNewEstateAgent = false;
                $scope.form.estateAgent = item;
                $scope.estateAgentDropDownVisible = true;
            }
        }

        $scope.estateagentselectedCancel = function () {
            $scope.addNewEstateAgent = false;
            $scope.estateAgentDropDownVisible = true;
        }

        $scope.estateagentselectedApply = function () {
            // remove new item if one was added before
            var arr = $scope.estateagents;
            var filteredElements = arr.filter(function (item, index) { return item.ID > -2; });
            $scope.estateagents = filteredElements;

            // add new item to the array
            $scope.estateagents.push($scope.newestateAgent);
            $scope.addNewEstateAgent = false;
            $scope.estateAgentDropDownVisible = true;

            $scope.selectedEstateAgent = $scope.newestateAgent.Name;
            $scope.selectedEstateAgentId = -2;
        }

        $scope.clearOccupierSelected = function () {
            $scope.occupier = {}; 
        };

        $scope.doneOccupierSelected = function () {
            $scope.showDoneButton = false;
            $scope.occupier = {};
        };

        $scope.addOccupierSelected = function () {
            $scope.form.Occupiers.push($scope.occupier);
            $scope.addOccupancyGridData = $scope.form.Occupiers;
            $scope.addOccupancyGridOptions.data = $scope.addOccupancyGridData;
            $scope.occupier = {};
        }

        $scope.deleteAddOccupierClicked = function (model) {
            // remove from $scope.form.Occupiers where firstname and surname are a match
            var arr = $scope.form.Occupiers;
            var filteredElements = arr.filter(function (item, index) { return item.Firstname !== model.Firstname && item.Surname !== model.Surname; });
            $scope.form.Occupiers = filteredElements;
            $scope.addOccupancyGridData = $scope.form.Occupiers;
            $scope.addOccupancyGridOptions.data = $scope.addOccupancyGridData;
        };

        $scope.editAddOccupierClicked = function (model) {
            $scope.occupier = model;
            $scope.showDoneButton = true;
        };

        $scope.landlordselected = function (item) {
            $scope.selectedLandlord = item.Name;
            $scope.selectedLandlordId = item.ID;
            $scope.showLandlordFields = true;
            if (item.ID == 0 ) {
                $scope.newLandlord =
                   {
                       ID: -2,
                       Name: '',
                       Mobile: '',
                       MobileOrLandline: '',
                       Email: '',
                       Address: '',
                       Note : ''
                   };
                $scope.form.landlord = $scope.newLandlord;
                $scope.addNewLandlord = true;
                $scope.landlordDropDownVisible = false;
            }
            else {
                $scope.addNewLnadlord = false;
                $scope.form.landlord = item;
                $scope.landlordDropDownVisible = true;
            }
        }

        $scope.landlordselectedCancel = function () {
            $scope.addNewLandlord = false;
            $scope.landlordDropDownVisible = true;
        }

        $scope.landlordselectedApply = function () {
            // remove new item if one was added before
            var arr = $scope.landlords;
            var filteredElements = arr.filter(function (item, index) { return item.ID > -2; });
            $scope.landlords = filteredElements;

            // add new item to the array
            $scope.landlords.push($scope.newLandlord);
            $scope.addNewLandlord = false;
            $scope.landlordDropDownVisible = true;

            $scope.selectedLandlord = $scope.newLandlord.Name;
            $scope.selectedLandlordId = -2;
        }

        $scope.addOccupancyClicked = function () {
            initForm();
            $("#myModal").modal();
        }

        $scope.deleteOccupancyClicked = function (model) {
            $scope.DeleteOccupancyID = model.ID;
            $("#confirmModal").modal();
        }

        $scope.deleteGetOccupancyYesClicked = function () {

            var id = $scope.DeleteOccupancyID;
            GetOccupancyService.deleteGetOccupancy(id)
               .then(function (response) {

                   if (response.data.success === true) {
                       $scope.GetOccupancies();
                   }
                   else {

                   }
               })
               .catch(function (response) {
                   $scope.status = 'Unable to delete occupancy ' + response.message;
               });
        }

        /********************************************************************  MODAL CRUD ******************************************************/

        $scope.updateOccupancy = function () {

            $scope.formEdit.ApartmentID = $scope.selectedApartmentId;

            OccupancyService.updateOccupancy($scope.formEdit)
                .then(function (response) {
                    if (response.data.success === true) {
                        ShowAlert('Occupancy was updated successfully.', 'success');
                        $scope.GetOccupancies();
                    }
                    else {
                        ShowAlert('Unable to update occupancy details.', 'error');
                    }
                })
                .catch(function (response) {
                    $scope.status = 'Unable to update occupancy ' + response.message;
                    console.log($scope.status);
                });
        }

        $scope.addOccupancy = function () {
            $scope.submitted = true;
            $scope.alerts = [];

            $scope.form.ApartmentID = $scope.selectedApartmentId;
            $scope.form.ApartmentTypeID = $scope.selectedApartmentTypeId;
            $scope.form.EstateAgentID = $scope.selectedEstateAgentId;
            $scope.form.LandlordID = $scope.selectedLandlordId;
            $scope.form.OccupierTypeID = $scope.selectedOccupierTypeId;
            $scope.vehicle1.Colour = $scope.ColourHex1;
            $scope.vehicle2.Colour = $scope.ColourHex2;
            $scope.form.Vehicles.push($scope.vehicle1);
            $scope.form.Vehicles.push($scope.vehicle2);

            if ($scope.selectedApartmentId == -1) {
                $scope.addAlert('danger', 'Please correct errors');
                $scope.addForm.dropdownApartment.$setValidity('required', false);
            }

            if ($scope.selectedOccupierTypeId == -1) {
                $scope.addAlert('danger', 'Please correct errors');
                $scope.addForm.dropdownOccupierType.$setValidity('required', false);
            }
             
            if ($scope.addForm.$valid) {
                OccupancyService.addOccupancy($scope.form)
                    .then(function (response) {
                        if (response.data.success === true) {
                            $scope.submitted = false;
                            $scope.addAlert('success', 'Occupancy was added successfully');
                            $scope.saveFooterVisible = false;
                            $scope.GetOccupancies();
                        }
                        else {
                            $scope.addAlert('danger', 'Unable to add Occupancy details');
                        }
                    })
                    .catch(function (response) {
                        $scope.status = 'Unable to add occupancy ' + response.message;
                        console.log($scope.status);
                    });
            }
            else {
                $scope.addAlert('danger', 'Please correct errors');
            }
        }
        
        $scope.editOccupancyClicked = function (model) {
            
            $scope.submitted = true;
            $scope.alerts = [];
            $scope.getAllApartments();

            //if ($scope.selectedApartmentTypeId == -1) {
            //    $scope.editForm.dropdownApartmentType.$setValidity('required', false);
            //}

           // $("#editOccupancyModal").modal();

            var id = model.ID; 
            OccupancyService.getOccupancy(id)
               .then(function (response) {
                   var oc = response.data.occupancy;

                   $scope.form = oc;
                   $scope.selectedApartmentId = oc.ApartmentID;
                   $scope.selectedApartment = oc.Apartment.ApartmentNo;

                   $scope.selectedApartmentTypeId = oc.ApartmentTypeID;
                   $scope.selectedApartmentType = oc.Apartment.ApartmentType.Name;
                          
                   $scope.selectedOccupierTypeId = oc.OccupierTypeID;
                   $scope.selectedOccupierType = oc.OccupierType.Name;

                   $scope.selectedEstateAgentId = oc.EstateAgentID;
                   $scope.selectedEstateAgent = oc.EstateAgent.Name;

                   $scope.selectedLandlordId = oc.LandlordID;
                   $scope.selectedLandlord = oc.Landlord.Name;

                   $scope.selectedOccupierStatus = "2";  // current by default
                   $scope.selectedOccupierStatusId = 2;

                   if (oc.OccupierTypeID > 0)
                   {
                       $scope.estateAgentDropDownVisible = true;
                       $scope.showEstateAgentFields = false;
                       $scope.landlordDropDownVisible = true;
                       $scope.showLandlordFields = false;
                   }

                   if (oc.Vehicles !== null && oc.Vehicles !== undefined) {
                       if (oc.Vehicles.length > 0) {
                           $scope.vehicle1 = oc.Vehicles[0];
                           $("#colorselector_2").colorselector("setColor", oc.Vehicles[0].Colour);
                       } // TO DO 
                       //if (oc.Vehicles.length > 1) { 
                       //    $scope.vehicle2 = oc.Vehicles[1];
                       //    $("#colorselector_2").colorselector("setColor", oc.Vehicles[1].Colour);
                       //}
                   }
                   $scope.form.DateOfOccupation = getJSONDateToDDMMYYY(oc.DateOfOccupation);
                   $scope.form.LeavingDate = getJSONDateToDDMMYYY(oc.LeavingDate);

                   // Occupiers TAB
                   $scope.addOccupancyGridData = $scope.form.Occupiers;
                   $scope.addOccupancyGridOptions.data = $scope.addOccupancyGridData;


                   //$scope.selectedCategoryId = response.data.occupancy.CategoryID;
                   //$scope.selectedCategory = response.data.occupancy.Category.Name;

                   //$scope.selectedFormatId = response.data.occupancy.FormatID;
                   //$scope.selectedFormat = response.data.occupancy.Format.Name;

                   //$scope.selectedSourceId = response.data.occupancy.SourceID;
                   //$scope.selectedSource = response.data.occupancy.Source.Name;

                   $("#editOccupancyModal").modal();
               })
               .catch(function (response) {
                   $scope.status = 'Unable to edit occupancy ' + response.message;
                   console.log($scope.status);
               });
        }

    }

]);

app.directive('dateTimePicker', dateTimePicker);