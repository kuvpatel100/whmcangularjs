﻿

app.controller("EstateAgentController", ['$http', '$scope', 'EstateAgentService', 'uiGridConstants',

    function ($http, $scope, EstateAgentService, uiGridConstants) {

        $scope.gridData;                        // grid date from service call is bound to the ui grid
        $scope.alerts = [];                     // alerts 
        $scope.saveFooterVisible = true;        // toggle footer buttons
        $scope.submitted = false;                // modal form submitted flag
        $scope.ph_numbr = /^0(\d ?){10}$/;
        $scope.eml_add = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;

        /********************************************************************  ALERTS AND MODAL EVENTS **************************************************************/

        $scope.addAlert = function (type, msg) {
            $scope.alerts.push({
                type: type,
                msg: msg
            })
        }

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        // reset validaiton when modal loads
        $('.modal').on('shown.bs.modal', function () {
            $scope.addForm.$setPristine();
        });

        // do cleanup when modal form is closed
        $('.modal').on('hidden.bs.modal', function () {
            $scope.alerts = [];                         // clear all alerts
            $scope.saveFooterVisible = true;            // set modal buttons to default
            $scope.submitted = false;
        });

        /********************************************************************  ANGULAR UI GRID ******************************************************/

        $scope.estateagentGridOptions = {
            paginationPageSizes: [10, 15, 20],
            paginationPageSize: 10,
            useExternalPagination: false,
            useExternalSorting: false,
            enableFiltering: true,
            enableSorting: true,
            enableRowSelection: true,
            enableSelectAll: true,
            enableGridMenu: true,
            columnDefs: [
              { name: 'ID' },
              { name: 'Name' },
              { name: 'ContactName' },
              { name: 'PhoneNumber' },
              { name: 'Email' },
              { name: 'Address' },
              {
                  name: 'Actions',
                  enableFiltering: false,
                  enableSorting: false,
                  width: '15%',
                  enableColumnResizing: false,
                  cellTemplate: '<div style="padding-top: 4px; text-align: center;"><button type="button" class="btn btn-success btn-xs" ng-click="grid.appScope.editEstateAgentClicked(row.entity)" id="btnEditEstateAgent">Edit <span class="glyphicon glyphicon-edit"> </button>&nbsp&nbsp' +
                                '<button type="button" class="btn btn-danger btn-xs" ng-click="grid.appScope.deleteEstateAgentClicked(row.entity)" id="btnDeleteEstateAgent">Delete <span class="glyphicon glyphicon-remove"></span></button></div>'
              }
            ]
        };

        $scope.getEstateAgents = function () {
            EstateAgentService.getEstateAgents()
                .then(function (response) {
                    $scope.gridData = response.data;
                    $scope.estateagentGridOptions.data = $scope.gridData;
                })
                .catch(function (response) {
                    $scope.status = 'Unable to load estateagent data: ' + response.message;
                    console.log($scope.status);
                });
        };

        $scope.getEstateAgents();

        /********************************************************************  MODAL INIT ******************************************************/

        function initForm() {

            $scope.form =
            {
                ID: 0,
                Name: "",
                ContactName: "",
                Email: "",
                PhoneNumber: "",
                Address: ""
            };
        }

        /********************************************************************  MODAL EVENTS ******************************************************/

        $scope.addEstateAgentClicked = function () {
            initForm();
            $scope.saveFooterVisible = true;
            $("#addEstateAgentModal").modal();
        }

        $scope.editEstateAgentClicked = function (model) {
            var id = model.ID;
            EstateAgentService.getEstateAgent(id)
               .then(function (response) {
                   $scope.form = response.data.estateagent;
                   $("#editEstateAgentModal").modal();
               })
               .catch(function (response) {
                   $scope.status = 'Unable to edit estateagent ' + response.message;
                   console.log($scope.status);
               });
        }

        $scope.deleteEstateAgentClicked = function (model) {
            $scope.DeleteEstateAgentID = model.ID;
            $("#confirmEstateAgentModal").modal();
        }

        $scope.deleteEstateAgentYesClicked = function () {

            var id = $scope.DeleteEstateAgentID;

            EstateAgentService.deleteEstateAgent(id)
               .then(function (response) {

                   if (response.data.success === true) {
                       $scope.addAlert('success', 'Estate Agent was deleted successfully');
                       $scope.getEstateAgents();
                   }
                   else {
                       $scope.addAlert('danger', 'Unable to delete EstateAgent details');
                   }
               })
               .catch(function (response) {
                   $scope.status = 'Unable to delete estateagent ' + response.message;
               });
        }

        /********************************************************************  MODAL CRUD ******************************************************/

        $scope.updateEstateAgent = function () {

            EstateAgentService.updateEstateAgent($scope.form)
                .then(function (response) {
                    if (response.data.success === true) {
                        $scope.addAlert('success', 'Estate Agent was updated successfully');
                        $scope.saveFooterVisible = false;
                        $scope.getEstateAgents();
                    }
                    else {
                        $scope.addAlert('danger', 'Unable to update EstateAgent details');
                    }
                })
                .catch(function (response) {
                    $scope.status = 'Unable to update estate agent ' + response.message;
                });
        }

        $scope.addEstateAgent = function () {
            $scope.submitted = true;
            $scope.alerts = [];

           if ($scope.addForm.$valid) {
                EstateAgentService.addEstateAgent($scope.form)
              .then(function (response) {
                  if (response.data.success === true) {
                      $scope.submitted = false;
                      $scope.addAlert('success', 'Estate Agent was added successfully');
                      $scope.saveFooterVisible = false;
                      $scope.getEstateAgents();
                  }
                  else {
                      $scope.addAlert('danger', 'Unable to add EstateAgent details');
                  }
              })
              .catch(function (response) {
                  $scope.status = 'Unable to add estate agent ' + response.message;
                  console.log($scope.status);
              });

            }
            else {
                $scope.addAlert('danger', 'Please correct errors');
            }
        }
    }
]);
