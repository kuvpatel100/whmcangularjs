﻿
var app;
(function () {
    'use strict';
    app = angular.module('myapp',
        [
            'ngAnimate',                // support for CSS-based animations
            'ngTouch',                  //for touch-enabled devices
            'ui.grid',                  //data grid for AngularJS
            'ui.grid.pagination',       //data grid Pagination
            'ui.grid.resizeColumns',    //data grid Resize column
            'ui.grid.moveColumns',      //data grid Move column
            'ui.grid.pinning',          //data grid Pin column Left/Right
            'ui.grid.selection',        //data grid Select Rows
            'ui.grid.autoResize',       //data grid Enabled auto column Size
            'ui.grid.exporter',         //data grid Export Data
            'ngSanitize',
            'ngMessages',
            'ui.bootstrap'
        ]);
})();


app.run(function ($rootScope) {
    $rootScope.PenthouseNumbers = 80;
});

var getJSONDateToDDMMYYY = function (date) {
    // JSON date to javascript date
    var input = new Date(date.match(/\d+/)[0] * 1);

    var dd = input.getDate();
    var mm = input.getMonth() + 1;
    var yyyy = input.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    var result = dd + '/' + mm + '/' + yyyy;

    if (result === '01/01/3939') {
        result = '';
    }

    return result;
}

// javascript date to DDMMYYY
var getDisplayDateTODDMMYYY = function (input) {

    var dd = input.getDate();
    var mm = input.getMonth() + 1;
    var yyyy = input.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    var result = dd + '/' + mm + '/' + yyyy;
    return result;
}

var dateTimePicker = function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, element, attrs, ngModelCtrl) {
            var parent = $(element).parent();
            var dtp = parent.datepicker({
                format: "dd/mm/yyyy",
                format: {
                    /*
                     * Say our UI should display a week ahead,
                     * but textbox should store the actual date.
                     * This is useful if we need UI to select local dates,
                     * but store in UTC
                     */
                    toDisplay: function (date, format, language) {
                        var result = getDisplayDateTODDMMYYY(date);
                        return result;
                    },
                    toValue: function (date, format, language) {
                        var d = new Date(date);
                        d.setDate(d.getDate() + 7);
                        return new Date(d);
                    }
                },

                showTodayButton: true,
                showOnFocus: false,
                todayHighlight: true,
                autoclose: true

            });
            dtp.on("dp.change", function (e) {
                ngModelCtrl.$setViewValue(moment(e.date).format("dd/mm/yyyy"));
                scope.$apply(); 
            });
        }
    };
};

app.directive('dateTimePicker', dateTimePicker);


app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return ''
                var onlyNumeric = inputValue.replace(/[^0-9]/g, '');
                if (onlyNumeric != inputValue) {
                    modelCtrl.$setViewValue(onlyNumeric);
                    modelCtrl.$render();
                }
                return onlyNumeric;
            });
        }
    };
});

//app.directive('ngCompare', function () {
//    return {
//        require: 'ngModel',
//        link: function (scope, currentEl, attrs, ctrl) {
//            var comparefield = document.getElementsByName(attrs.ngCompare)[0]; //getting first element
//            compareEl = angular.element(comparefield);
 
//            //current field key up
//            currentEl.on('keyup', function () {
//                if (compareEl.val() != "") {
//                    var isMatch = currentEl.val() === compareEl.val();
//                    ctrl.$setValidity('compare', isMatch);
//                    scope.$digest();
//                }
//            });
//            //Element to compare field key up
//            compareEl.on('keyup', function () {
//                if (currentEl.val() != "") {
//                    var isMatch = currentEl.val() === compareEl.val();
//                    ctrl.$setValidity('compare', isMatch);
//                    scope.$digest();
//                }
//            });
//        }
//    }
//});