﻿
app.service('ApartmentService', ['$http', function ($http) {

    this.updateApartment = function (formData) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/Apartment/Update', formData);
    };

    this.addApartment = function (formData) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/Apartment/Add', formData);
    };

    this.getApartment = function (id) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/Apartment/Get/' + id);
    };

    this.deleteApartment = function (id) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/Apartment/Delete/' + id);
    };

    this.getApartments = function () {
        //var response;
        return $http.post('/Apartment/GetAll/');
    };

    // used in dropdown fornew occupancy
    this.getApartmentList = function () {
        return $http.post('/Apartment/GetApartmentList/');
    };


}]);
