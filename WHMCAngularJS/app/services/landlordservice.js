﻿
app.service('LandlordService', ['$http', function ($http) {

    this.updateLandlord = function (formData) {
        return $http.post('/Landlord/Update', formData);
    };

    this.addLandlord = function (formData) {
        return $http.post('/Landlord/Add', formData);
    };

    this.getLandlord = function (id) {
        return $http.post('/Landlord/GetByID/' + id);
    };

    this.deleteLandlord = function (id) {
        return $http.post('/Landlord/Delete/' + id);
    };

    this.getLandlords = function () {
        //var response;
        return $http.post('/Landlord/GetAll/');
    };

    this.getLandlordList = function (pageNumber, pageSize) {
        var param = $.param({
            pageNumber: pageNumber,
            pageSize: pageSize
        });
        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        return $http.post('/Landlord/GetByPage/', param, config);
    };

}]);
