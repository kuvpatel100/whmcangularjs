﻿
app.service('OccupierTypeService', ['$http', function ($http) {

    this.updateOccupierType = function (formData) {
        return $http.post('/Home/Update', formData);
    };

    this.addOccupierType = function (formData) {
        return $http.post('/Home/Add', formData);
    };

    this.getOccupierType = function (id) {
        return $http.post('/Home/Get/' + id);
    };

    this.deleteOccupierType = function (id) {
        return $http.post('/Home/Delete/' + id);
    };

    this.getOccupierTypes = function (pageNumber, pageSize) {
        var param = $.param({
            pageNumber: pageNumber,
            pageSize: pageSize
        });
        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        return $http.post('/OccupierType/GetAll/', param, config);
    };

    this.getOccupierStatus = function () {
        var types = [
            { ID: "1", Name: "Previous" },
            { ID: "2", Name: "Current" }
        ];
        return types;
    };

}]);
