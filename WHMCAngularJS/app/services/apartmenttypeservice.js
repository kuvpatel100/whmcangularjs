﻿
app.service('ApartmentTypeService', ['$http', function ($http) {

    this.updateApartmentType = function (formData) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/ApartmentType/Update', formData);
    };

    this.addApartmentType = function (formData) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/ApartmentType/Add', formData);
    };

    this.getApartmentType = function (id) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/ApartmentType/Get/' + id);
    };

    this.deleteApartmentType = function (id) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/ApartmentType/Delete/' + id);
    };

    this.getApartmentTypes = function () {
        //var response;
        return $http.post('/ApartmentType/GetAll/');
    };

}]);
