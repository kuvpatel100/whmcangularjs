﻿
app.service('OccupancyNoteTypeService', ['$http', function ($http) {

    this.updateOccupancyNoteType = function (formData) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/OccupancyNoteType/Update', formData);
    };

    this.addOccupancyNoteType = function (formData) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/OccupancyNoteType/Add', formData);
    };

    this.getOccupancyNoteType = function (id) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/OccupancyNoteType/Get/' + id);
    };

    this.deleteOccupancyNoteType = function (id) {
        // return a Promise object so that the caller can handle success/failure
        //return $http({ method: 'POST', url: '/Film/SaveFilm', data: formData });
        return $http.post('/OccupancyNoteType/Delete/' + id);
    };

    this.getOccupancyNoteTypes = function () {
        //var response;
        return $http.post('/OccupancyNoteType/GetAll/');
    };

    // used in dropdown fornew occupancy
    this.getOccupancyNoteTypeList = function () {
        return $http.post('/OccupancyNoteType/GetNoteList/');
    };


}]);
