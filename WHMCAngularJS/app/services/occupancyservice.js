﻿
app.service('OccupancyService', ['$http', function ($http) {

    this.updateOccupancy = function (formData) {
        return $http.post('/Occupancy/Update', formData);
    };

    this.addOccupancy = function (formData) {
        return $http.post('/Occupancy/Add', formData);
    };

    this.getOccupancy = function (id) {
        return $http.post('/Occupancy/GetByID/' + id);
    };

    this.deleteOccupancy = function (id) {
        return $http.post('/Occupancy/Delete/' + id);
    };

    this.getOccupancies = function (pageNumber, pageSize) {
        var param = $.param({
            pageNumber: pageNumber,
            pageSize: pageSize
        });
        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        return $http.post('/Occupancy/GetByPage/', param, config);
    };



}]);
