﻿
app.service('EstateAgentService', ['$http', function ($http) {

    this.updateEstateAgent = function (formData) {
        return $http.post('/EstateAgent/Update', formData);
    };

    this.addEstateAgent = function (formData) {
        return $http.post('/EstateAgent/Add', formData);
    };

    this.getEstateAgent = function (id) {
        return $http.post('/EstateAgent/GetByID/' + id);
    };

    this.deleteEstateAgent = function (id) {
        return $http.post('/EstateAgent/Delete/' + id);
    };

    this.getEstateAgents = function () {
        //var response;
        return $http.post('/EstateAgent/GetAll/');
    };

    this.getEstateAgentList = function (pageNumber, pageSize) {
        var param = $.param({
            pageNumber: pageNumber,
            pageSize: pageSize
        });
        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        return $http.post('/EstateAgent/GetByPage/', param, config);
    };

}]);
