﻿CREATE TABLE [dbo].[Occupier]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Firstname] NCHAR(10) NULL, 
    [Surname] NCHAR(10) NULL, 
	[IsPrimary] BIT NOT NULL DEFAULT 0,
    [Deleted] BIT NULL DEFAULT 0, 
    [DateCreated] DATETIME NULL DEFAULT getdate(), 
    [DateUpdated] DATETIME NULL DEFAULT getdate(), 
    [OccupancyID] INT NOT NULL
    
)
