﻿CREATE TABLE [dbo].[OccupancyNote]
(
	[ID] INT NOT NULL PRIMARY KEY, 
    [Header] NVARCHAR(100) NULL, 
    [Note] NTEXT NULL, 
	[OccupancyID] INT NOT NULL,
    [DateCreated] DATETIME NULL DEFAULT getdate(), 
    [DateUpdated] DATETIME NULL DEFAULT getdate()
)
