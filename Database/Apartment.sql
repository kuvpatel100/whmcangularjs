﻿CREATE TABLE [dbo].[Apartment]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ApartmentNo] VARCHAR(50) NOT NULL, 
    [FloorNo] INT NULL, 
    [NoOfBedrooms] INT NULL, 
    [ApartmentTypeID] INT NOT NULL DEFAULT 1, 
    [ParkingSpaceNo] INT NULL DEFAULT 0, 
	[Note] NTEXT NULL,
    [DateCreated] DATETIME NOT NULL DEFAULT getdate(), 
    [DateUpdated] DATETIME NOT NULL DEFAULT getdate()
    
)
