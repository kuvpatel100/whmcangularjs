﻿CREATE TABLE [dbo].[ApartmentType]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NULL, 
    [Deleted] BIT NULL, 
    [DateCreated] DATETIME NULL DEFAULT getdate(), 
    [DateUpdated] DATETIME NULL DEFAULT getdate()
)
