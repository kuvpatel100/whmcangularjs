﻿CREATE TABLE [dbo].[EstateAgent]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(100) NULL, 
    [ContactName] NVARCHAR(50) NULL, 
    [PhoneNumber] NVARCHAR(25) NULL, 
    [Email] NVARCHAR(100) NULL, 
	[Address] NTEXT NULL,
    [DateCreated] DATETIME NOT NULL DEFAULT getdate(), 
    [DateUpdated] DATETIME NOT NULL DEFAULT getdate(), 
    [Deleted] BIT NULL DEFAULT 0
    
)
