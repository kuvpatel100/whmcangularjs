﻿CREATE TABLE [dbo].[Landlord]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(100) NULL, 
    [Mobile] NVARCHAR(25) NULL, 
	[MobileOrLandline] NVARCHAR(25) NULL, 
    [Email] NVARCHAR(100) NULL, 
	[Address] NTEXT NULL,
    [DateCreated] DATETIME NOT NULL DEFAULT getdate(), 
    [DateUpdated] DATETIME NOT NULL DEFAULT getdate(), 
    [Deleted] BIT NULL DEFAULT 0, 
    [Note] NTEXT NULL
)
