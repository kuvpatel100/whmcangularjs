﻿CREATE TABLE [dbo].[OccupierType]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY, 
	[Name] NVARCHAR(30) NULL, 
    [Deleted] BIT NULL, 
    [DateCreated] DATETIME NULL DEFAULT getdate(), 
    [DateUpdated] DATETIME NULL DEFAULT getdate()
)
