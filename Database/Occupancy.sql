﻿CREATE TABLE [dbo].[Occupancy]
(
	[ID] INT IDENTITY (1, 1) NOT NULL,
    [ApartmentID] INT NULL, 
    [ParkingSpaceNo] INT NULL, 
    [DateOfOccupation] DATETIME NULL, 
    [LeavingDate] DATETIME NOT NULL, 
    [OccupierTypeID] INT NOT NULL, 
    [LandlordID] INT NULL, 
    [EstateAgentID] INT NULL, 
    [Email] NVARCHAR(150) NULL, 
    [Mobile] NVARCHAR(20) NULL, 
    [MobileOrLandline] NVARCHAR(20) NULL, 
    [Deleted] BIT NOT NULL DEFAULT 0, 
    [DateCreated] DATETIME NULL DEFAULT getdate(), 
    [DateUpdated] DATETIME NULL DEFAULT getdate(), 
    CONSTRAINT [PK_Occupancy] PRIMARY KEY ([ID]) 
)
